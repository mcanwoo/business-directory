<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class photo extends Model
{
    //

 protected $table = 'photos';

public  $timestamps = false;

 protected $guarded = ['id'];
protected $fillable = ['location','name','company_id' ];



}
