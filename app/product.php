<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
 protected $table = 'products';

public  $timestamps = false;

 protected $guarded = ['id'];
protected $fillable = ['description','name','company_id','price' ,'photos'];

}
