<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    //
     protected $table = 'services';

public  $timestamps = false;

 protected $guarded = ['id'];
protected $fillable = ['description','name','company_id','price','photos', 'class' ];
}
