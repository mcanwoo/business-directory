<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class company extends Model
{
use Searchable;
	
 protected $table = 'companies';

public  $timestamps = false;

 protected $guarded = ['id'];
protected $fillable = ['description','name','phone1','phone2','fax','email','website','adress','directions','logo','vizyon', 'misyon','facebook', 'twitter','instagram','promo','workstart_hour','workend_hour','tags','fields'];

    //

public function photos()
    {
        return $this->hasMany('App\photo');
    }
public function brands()
    {
        return $this->hasMany('App\brand');
    }
    public function comments()
    {
        return $this->hasMany('App\comment');
    }
        public function products()
    {
        return $this->hasMany('App\product');
    }
        public function services()
    {
        return $this->hasMany('App\service');
    }
        public function promotion()
    {
        return $this->hasMany('App\promotion');
    }


}
