<?php

namespace App\Http\Controllers;

use App\promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class promotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          //


          //
        function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}





$name= "default.jpg";

if(Input::hasfile('logo'))
         {

            
  $name= generateRandomString() . ".jpg";
               



               $request->logo->move('visuals', $name);  
                // $data[] = $name;  
            }


 promotion::create([
                'company_id' => $request->input('company_id'),
                'name'=>$request->input('name'),
                'description' => $request->input('description'),
                'end_date' => $request->input('end_date')
            ]);



return redirect('addpromotion');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(promotion $promotion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, promotion $promotion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(promotion $promotion,$id)
    {
        //                
          promotion::destroy($id);
  $comments = promotion::all();
  
         return view('comments', compact('comments'));
    }
}
