<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\company;
use App\photo;
use App\brand;
use App\product;
use App\service;
use App\promotion;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = company::all();
        return view('home', compact('companies'));
    }
     public function photos()
    {
        $companies = company::all();
        return view('photo', compact('companies'));
    }

  public function brands()
    {
        $companies = company::all();
        $brands = brand::all();
        return view('brand', compact('brands','companies'));
    }

  public function product()
    {
        $companies = company::all();
        $products = product::all();
        return view('product', compact('products','companies'));
    }

  public function service()
    {
        $companies = company::all();
        $services = service::all();
        return view('service', compact('services','companies'));
    }

  public function promotion()
    {
        $companies = company::all();
        $promotions = promotion::all();
        return view('promotion', compact('promotions','companies'));
    }
    

    public function update($id,Request $request)
    {

$company = company::findOrfail($id);

$company->name = $request->name;
$company->description = $request->description;
$company->misyon = $request->misyon;
$company->vizyon = $request->vizyon;
$company->website = $request->website;
$company->phone1 = $request->phone1;
$company->phone2 = $request->phone2;
$company->facebook = $request->facebook;
$company->twitter = $request->twitter;
$company->instagram = $request->instagram;
$company->adress = $request->adress;
$company->directions = $request->directions;
$company->tags = $request->tags;   
$company->fields = $request->fields;   
$company->fax = $request->fax;   
$company->email = $request->email;   

$company->save();

 return view('edit', compact('company'));


}


    public function edit($id)
    {

$company = company::findOrfail($id);

 return view('edit', compact('company'));


}
}
