<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class promotion extends Model
{
    //
     protected $table = 'promotions';

public  $timestamps = false;

 protected $guarded = ['id'];
protected $fillable = ['description','name','company_id','end_date' ,'photos'];
}
