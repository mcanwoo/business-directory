<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    //
 protected $table = 'comments';

 protected $guarded = ['id'];
protected $fillable = ['author','content','email','status','company_id','rating' ];


}
