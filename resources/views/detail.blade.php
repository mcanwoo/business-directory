<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Travelo - Travel, Tour Booking HTML5 Template</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">
<meta name="_token" content="{{csrf_token()}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/html/favicon.ico" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="/html/css/bootstrap.min.css">
    <link rel="stylesheet" href="/html/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/html/css/animate.min.css">
     <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="/html/components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/flexslider/flexslider.css" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="/html/css/style.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="/html/css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="/html/css/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="/html/css/responsive.css">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="/html/css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="/html/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="/html/http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="/html/#">MY ACCOUNT</a></li>
                        <li class="ribbon">
                            <a href="/html/#">English</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="Dansk">Dansk</a></li>
                                <li><a href="/html/#" title="Deutsch">Deutsch</a></li>
                                <li class="active"><a href="/html/#" title="English">English</a></li>
                                <li><a href="/html/#" title="Español">Español</a></li>
                                <li><a href="/html/#" title="Français">Français</a></li>
                                <li><a href="/html/#" title="Italiano">Italiano</a></li>
                                <li><a href="/html/#" title="Magyar">Magyar</a></li>
                                <li><a href="/html/#" title="Nederlands">Nederlands</a></li>
                                <li><a href="/html/#" title="Norsk">Norsk</a></li>
                                <li><a href="/html/#" title="Polski">Polski</a></li>
                                <li><a href="/html/#" title="Português">Português</a></li>
                                <li><a href="/html/#" title="Suomi">Suomi</a></li>
                                <li><a href="/html/#" title="Svenska">Svenska</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <li><a href="/html/#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="/html/#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency">
                            <a href="/html/#" title="">USD</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="AUD">AUD</a></li>
                                <li><a href="/html/#" title="BRL">BRL</a></li>
                                <li class="active"><a href="/html/#" title="USD">USD</a></li>
                                <li><a href="/html/#" title="CAD">CAD</a></li>
                                <li><a href="/html/#" title="CHF">CHF</a></li>
                                <li><a href="/html/#" title="CNY">CNY</a></li>
                                <li><a href="/html/#" title="CZK">CZK</a></li>
                                <li><a href="/html/#" title="DKK">DKK</a></li>
                                <li><a href="/html/#" title="EUR">EUR</a></li>
                                <li><a href="/html/#" title="GBP">GBP</a></li>
                                <li><a href="/html/#" title="HKD">HKD</a></li>
                                <li><a href="/html/#" title="HUF">HUF</a></li>
                                <li><a href="/html/#" title="IDR">IDR</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="main-header">
                
                <a href="/html/#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="/html/index.html" title="Travelo - home">
                            <img src="/html/images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="/html/index.html">Home</a>
                                <ul>
                                    <li><a href="/html/index.html">Home Layout 1</a></li>
                                    <li><a href="/html/homepage2.html">Home Layout 2</a></li>
                                    <li><a href="/html/homepage3.html">Home Layout 3</a></li>
                                    <li><a href="/html/homepage4.html">Home Layout 4</a></li>
                                    <li><a href="/html/homepage5.html">Home Layout 5</a></li>
                                    <li><a href="/html/homepage6.html">Home Layout 6</a></li>
                                    <li><a href="/html/homepage7.html">Home Layout 7</a></li>
                                    <li><a href="/html/homepage8.html">Home Layout 8</a></li>
                                    <li><a href="/html/homepage9.html">Home Layout 9</a></li>
                                    <li><a href="/html/homepage10.html">Home Layout 10</a></li>
                                    <li><a href="/html/homepage11.html">Home Layout 11</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/hotel-index.html">Hotels</a>
                                <ul>
                                    <li><a href="/html/hotel-index.html">Home Hotels</a></li>
                                    <li><a href="/html/hotel-list-view.html">List View</a></li>
                                    <li><a href="/html/hotel-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/hotel-block-view.html">Block View</a></li>
                                    <li><a href="/html/hotel-detailed.html">Detailed</a></li>
                                    <li><a href="/html/hotel-booking.html">Booking</a></li>
                                    <li><a href="/html/hotel-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/flight-index.html">Flights</a>
                                <ul>
                                    <li><a href="/html/flight-index.html">Home Flights</a></li>
                                    <li><a href="/html/flight-list-view.html">List View</a></li>
                                    <li><a href="/html/flight-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/flight-block-view.html">Block View</a></li>
                                    <li><a href="/html/flight-detailed.html">Detailed</a></li>
                                    <li><a href="/html/flight-booking.html">Booking</a></li>
                                    <li><a href="/html/flight-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/car-index.html">Cars</a>
                                <ul>
                                    <li><a href="/html/car-index.html">Home Cars</a></li>
                                    <li><a href="/html/car-list-view.html">List View</a></li>
                                    <li><a href="/html/car-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/car-block-view.html">Block View</a></li>
                                    <li><a href="/html/car-detailed.html">Detailed</a></li>
                                    <li><a href="/html/car-booking.html">Booking</a></li>
                                    <li><a href="/html/car-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/cruise-index.html">Cruises</a>
                                <ul>
                                    <li><a href="/html/cruise-index.html">Home Cruises</a></li>
                                    <li><a href="/html/cruise-list-view.html">List View</a></li>
                                    <li><a href="/html/cruise-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/cruise-block-view.html">Block View</a></li>
                                    <li><a href="/html/cruise-detailed.html">Detailed</a></li>
                                    <li><a href="/html/cruise-booking.html">Booking</a></li>
                                    <li><a href="/html/cruise-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/tour-index.html">Tour</a>
                                <ul>
                                    <li><a href="/html/tour-index.html">Home Tour</a></li>
                                    <li><a href="/html/tour-fancy-package-2column.html">Fancy Packages 2 Column</a></li>
                                    <li><a href="/html/tour-fancy-package-3column.html">Fancy Packages 3 Column</a></li>
                                    <li><a href="/html/tour-fancy-package-4column.html">Fancy Packages 4 Column</a></li>
                                    <li><a href="/html/tour-simple-package-2column.html">Simple Packages 2 Column</a></li>
                                    <li><a href="/html/tour-simple-package-3column.html">Simple Packages 3 Column</a></li>
                                    <li><a href="/html/tour-simple-package-4column.html">Simple Packages 4 Column</a></li>
                                    <li><a href="/html/tour-simple-package-3column.html">Location - Eruope</a></li>
                                    <li><a href="/html/tour-simple-package-4column.html">Location - North America</a></li>
                                    <li><a href="/html/tour-detailed1.html">Detailed 1</a></li>
                                    <li><a href="/html/tour-detailed2.html">Detailed 2</a></li>
                                    <li><a href="/html/tour-booking.html">Booking</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children megamenu-menu">
                                <a href="/html/#">Pages</a>
                                <div class="megamenu-wrapper container" data-items-per-column="8">
                                    <div class="megamenu-holder">
                                        <ul class="megamenu">
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Standard Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/pages-aboutus1.html">About Us 1</a></li>
                                                    <li><a href="/html/pages-aboutus2.html">About Us 2</a></li>
                                                    <li><a href="/html/pages-services1.html">Services 1</a></li>
                                                    <li><a href="/html/pages-services2.html">Services 2</a></li>
                                                    <li><a href="/html/pages-photogallery-4column.html">Gallery 4 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-3column.html">Gallery 3 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-2column.html">Gallery 2 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-fullview.html">Gallery Read</a></li>
                                                    <li><a href="/html/pages-blog-rightsidebar.html">Blog Right Sidebar</a></li>
                                                    <li><a href="/html/pages-blog-leftsidebar.html">Blog Left Sidebar</a></li>
                                                    <li><a href="/html/pages-blog-fullwidth.html">Blog Full Width</a></li>
                                                    <li><a href="/html/pages-blog-read.html">Blog Read</a></li>
                                                    <li><a href="/html/pages-faq1.html">Faq 1</a></li>
                                                    <li><a href="/html/pages-faq2.html">Faq 2</a></li>
                                                    <li><a href="/html/pages-layouts-leftsidebar.html">Layouts Left Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-rightsidebar.html">Layouts Right Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-twosidebar.html">Layouts Two Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-fullwidth.html">Layouts Full Width</a></li>
                                                    <li><a href="/html/pages-contactus1.html">Contact Us 1</a></li>
                                                    <li><a href="/html/pages-contactus2.html">Contact Us 2</a></li>
                                                    <li><a href="/html/pages-contactus3.html">Contact Us 3</a></li>
                                                    <li><a href="/html/pages-travelo-policies.html">Travelo Policies</a></li>
                                                    <li><a href="/html/pages-sitemap.html">Site Map</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Extra Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/extra-pages-holidays.html">Holidays</a></li>
                                                    <li><a href="/html/extra-pages-hotdeals.html">Hot Deals</a></li>
                                                    <li><a href="/html/extra-pages-before-you-fly.html">Before You Fly</a></li>
                                                    <li><a href="/html/extra-pages-inflight-experience.html">Inflight Experience</a></li>
                                                    <li><a href="/html/extra-pages-things-todo1.html">Things To Do 1</a></li>
                                                    <li><a href="/html/extra-pages-things-todo2.html">Things To Do 2</a></li>
                                                    <li><a href="/html/extra-pages-travel-essentials.html">Travel Essentials</a></li>
                                                    <li><a href="/html/extra-pages-travel-stories.html">Travel Stories</a></li>
                                                    <li><a href="/html/extra-pages-travel-guide.html">Travel Guide</a></li>
                                                    <li><a href="/html/extra-pages-travel-ideas.html">Travel Ideas</a></li>
                                                    <li><a href="/html/extra-pages-travel-insurance.html">Travel Insurance</a></li>
                                                    <li><a href="/html/extra-pages-group-booking.html">Group Bookings</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Special Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/pages-404-1.html">404 Page 1</a></li>
                                                    <li><a href="/html/pages-404-2.html">404 Page 2</a></li>
                                                    <li><a href="/html/pages-404-3.html">404 Page 3</a></li>
                                                    <li><a href="/html/pages-coming-soon1.html">Coming Soon 1</a></li>
                                                    <li><a href="/html/pages-coming-soon2.html">Coming Soon 2</a></li>
                                                    <li><a href="/html/pages-coming-soon3.html">Coming Soon 3</a></li>
                                                    <li><a href="/html/pages-loading1.html">Loading Page 1</a></li>
                                                    <li><a href="/html/pages-loading2.html">Loading Page 2</a></li>
                                                    <li><a href="/html/pages-loading3.html">Loading Page 3</a></li>
                                                    <li><a href="/html/pages-login1.html">Login Page 1</a></li>
                                                    <li><a href="/html/pages-login2.html">Login Page 2</a></li>
                                                    <li><a href="/html/pages-login3.html">Login Page 3</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/#">Shortcodes</a>
                                <ul>
                                    <li><a href="/html/shortcode-accordions-toggles.html">Accordions &amp; Toggles</a></li>
                                    <li><a href="/html/shortcode-tabs.html">Tabs</a></li>
                                    <li><a href="/html/shortcode-buttons.html">Buttons</a></li>
                                    <li><a href="/html/shortcode-icon-boxes.html">Icon Boxes</a></li>
                                    <li><a href="/html/shortcode-gallery-styles.html">Image &amp; Gallery Styles</a></li>
                                    <li><a href="/html/shortcode-image-box-styles.html">Image Box Styles</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">Listing Styles</a>
                                        <ul>
                                            <li><a href="/html/shortcode-listing-style1.html">Listing Style 01</a></li>
                                            <li><a href="/html/shortcode-listing-style2.html">Listing Style 02</a></li>
                                            <li><a href="/html/shortcode-listing-style3.html">Listing Style 03</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/html/shortcode-dropdowns.html">Dropdowns</a></li>
                                    <li><a href="/html/shortcode-pricing-tables.html">Pricing Tables</a></li>
                                    <li><a href="/html/shortcode-testimonials.html">Testimonials</a></li>
                                    <li><a href="/html/shortcode-our-team.html">Our Team</a></li>
                                    <li><a href="/html/shortcode-gallery-popup.html">Gallery Popup</a></li>
                                    <li><a href="/html/shortcode-map-popup.html">Map Popup</a></li>
                                    <li><a href="/html/shortcode-style-changer.html">Style Changer</a></li>
                                    <li><a href="/html/shortcode-typography.html">Typography</a></li>
                                    <li><a href="/html/shortcode-animations.html">Animations</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/#">Bonus</a>
                                <ul>
                                    <li><a href="/html/dashboard1.html">Dashboard 1</a></li>
                                    <li><a href="/html/dashboard2.html">Dashboard 2</a></li>
                                    <li><a href="/html/dashboard3.html">Dashboard 3</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">7 Footer Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/footer-style1.html">Footer Style 1</a></li>
                                            <li><a href="/html/footer-style2.html">Footer Style 2</a></li>
                                            <li><a href="/html/footer-style3.html">Footer Style 3</a></li>
                                            <li><a href="/html/footer-style4.html">Footer Style 4</a></li>
                                            <li><a href="/html/footer-style5.html">Footer Style 5</a></li>
                                            <li><a href="/html/footer-style6.html">Footer Style 6</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">8 Header Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/header-style1.html">Header Style 1</a></li>
                                            <li><a href="/html/header-style2.html">Header Style 2</a></li>
                                            <li><a href="/html/header-style3.html">Header Style 3</a></li>
                                            <li><a href="/html/header-style4.html">Header Style 4</a></li>
                                            <li><a href="/html/header-style5.html">Header Style 5</a></li>
                                            <li><a href="/html/header-style6.html">Header Style 6</a></li>
                                            <li><a href="/html/header-style7.html">Header Style 7</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">7 Inner Start Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/inner-starts-style1.html">Inner Start Style 1</a></li>
                                            <li><a href="/html/inner-starts-style2.html">Inner Start Style 2</a></li>
                                            <li><a href="/html/inner-starts-style3.html">Inner Start Style 3</a></li>
                                            <li><a href="/html/inner-starts-style4.html">Inner Start Style 4</a></li>
                                            <li><a href="/html/inner-starts-style5.html">Inner Start Style 5</a></li>
                                            <li><a href="/html/inner-starts-style6.html">Inner Start Style 6</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">3 Search Styles</a>
                                        <ul>
                                            <li><a href="/html/search-style1.html">Search Style 1</a></li>
                                            <li><a href="/html/search-style2.html">Search Style 2</a></li>
                                            <li><a href="/html/search-style3.html">Search Style 3</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="/html/index.html">Home</a>
                            <ul>
                                <li><a href="/html/index.html">Home Layout 1</a></li>
                                <li><a href="/html/homepage2.html">Home Layout 2</a></li>
                                <li><a href="/html/homepage3.html">Home Layout 3</a></li>
                                <li><a href="/html/homepage4.html">Home Layout 4</a></li>
                                <li><a href="/html/homepage5.html">Home Layout 5</a></li>
                                <li><a href="/html/homepage6.html">Home Layout 6</a></li>
                                <li><a href="/html/homepage7.html">Home Layout 7</a></li>
                                <li><a href="/html/homepage8.html">Home Layout 8</a></li>
                                <li><a href="/html/homepage9.html">Home Layout 9</a></li>
                                <li><a href="/html/homepage10.html">Home Layout 10</a></li>
                                <li><a href="/html/homepage11.html">Home Layout 11</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/hotel-index.html">Hotels</a>
                            <ul>
                                <li><a href="/html/hotel-index.html">Home Hotels</a></li>
                                <li><a href="/html/hotel-list-view.html">List View</a></li>
                                <li><a href="/html/hotel-grid-view.html">Grid View</a></li>
                                <li><a href="/html/hotel-block-view.html">Block View</a></li>
                                <li><a href="/html/hotel-detailed.html">Detailed</a></li>
                                <li><a href="/html/hotel-booking.html">Booking</a></li>
                                <li><a href="/html/hotel-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/flight-index.html">Flights</a>
                            <ul>
                                <li><a href="/html/flight-index.html">Home Flights</a></li>
                                <li><a href="/html/flight-list-view.html">List View</a></li>
                                <li><a href="/html/flight-grid-view.html">Grid View</a></li>
                                <li><a href="/html/flight-block-view.html">Block View</a></li>
                                <li><a href="/html/flight-detailed.html">Detailed</a></li>
                                <li><a href="/html/flight-booking.html">Booking</a></li>
                                <li><a href="/html/flight-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/car-index.html">Cars</a>
                            <ul>
                                <li><a href="/html/car-index.html">Home Cars</a></li>
                                <li><a href="/html/car-list-view.html">List View</a></li>
                                <li><a href="/html/car-grid-view.html">Grid View</a></li>
                                <li><a href="/html/car-block-view.html">Block View</a></li>
                                <li><a href="/html/car-detailed.html">Detailed</a></li>
                                <li><a href="/html/car-booking.html">Booking</a></li>
                                <li><a href="/html/car-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/cruise-index.html">Cruises</a>
                            <ul>
                                <li><a href="/html/cruise-index.html">Home Cruises</a></li>
                                <li><a href="/html/cruise-list-view.html">List View</a></li>
                                <li><a href="/html/cruise-grid-view.html">Grid View</a></li>
                                <li><a href="/html/cruise-block-view.html">Block View</a></li>
                                <li><a href="/html/cruise-detailed.html">Detailed</a></li>
                                <li><a href="/html/cruise-booking.html">Booking</a></li>
                                <li><a href="/html/cruise-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/tour-index.html">Tour</a>
                            <ul>
                                <li><a href="/html/tour-index.html">Home Tour</a></li>
                                <li><a href="/html/tour-fancy-package-2column.html">Fancy Packages 2 Column</a></li>
                                <li><a href="/html/tour-fancy-package-3column.html">Fancy Packages 3 Column</a></li>
                                <li><a href="/html/tour-fancy-package-4column.html">Fancy Packages 4 Column</a></li>
                                <li><a href="/html/tour-simple-package-2column.html">Simple Packages 2 Column</a></li>
                                <li><a href="/html/tour-simple-package-3column.html">Simple Packages 3 Column</a></li>
                                <li><a href="/html/tour-simple-package-4column.html">Simple Packages 4 Column</a></li>
                                <li><a href="/html/tour-simple-package-3column.html">Location - Eruope</a></li>
                                <li><a href="/html/tour-simple-package-4column.html">Location - North America</a></li>
                                <li><a href="/html/tour-detailed1.html">Detailed 1</a></li>
                                <li><a href="/html/tour-detailed2.html">Detailed 2</a></li>
                                <li><a href="/html/tour-booking.html">Booking</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Pages</a>
                            <ul>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Standard Pages</a>
                                    <ul>
                                        <li><a href="/html/pages-aboutus1.html">About Us 1</a></li>
                                        <li><a href="/html/pages-aboutus2.html">About Us 2</a></li>
                                        <li><a href="/html/pages-services1.html">Services 1</a></li>
                                        <li><a href="/html/pages-services2.html">Services 2</a></li>
                                        <li><a href="/html/pages-photogallery-4column.html">Gallery 4 Column</a></li>
                                        <li><a href="/html/pages-photogallery-3column.html">Gallery 3 Column</a></li>
                                        <li><a href="/html/pages-photogallery-2column.html">Gallery 2 Column</a></li>
                                        <li><a href="/html/pages-photogallery-fullview.html">Gallery Read</a></li>
                                        <li><a href="/html/pages-blog-rightsidebar.html">Blog Right Sidebar</a></li>
                                        <li><a href="/html/pages-blog-leftsidebar.html">Blog Left Sidebar</a></li>
                                        <li><a href="/html/pages-blog-fullwidth.html">Blog Full Width</a></li>
                                        <li><a href="/html/pages-blog-read.html">Blog Read</a></li>
                                        <li><a href="/html/pages-faq1.html">Faq 1</a></li>
                                        <li><a href="/html/pages-faq2.html">Faq 2</a></li>
                                        <li><a href="/html/pages-layouts-leftsidebar.html">Layouts Left Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-rightsidebar.html">Layouts Right Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-twosidebar.html">Layouts Two Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-fullwidth.html">Layouts Full Width</a></li>
                                        <li><a href="/html/pages-contactus1.html">Contact Us 1</a></li>
                                        <li><a href="/html/pages-contactus2.html">Contact Us 2</a></li>
                                        <li><a href="/html/pages-contactus3.html">Contact Us 3</a></li>
                                        <li><a href="/html/pages-travelo-policies.html">Travelo Policies</a></li>
                                        <li><a href="/html/pages-sitemap.html">Site Map</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Extra Pages</a>
                                    <ul>
                                        <li><a href="/html/extra-pages-holidays.html">Holidays</a></li>
                                        <li><a href="/html/extra-pages-hotdeals.html">Hot Deals</a></li>
                                        <li><a href="/html/extra-pages-before-you-fly.html">Before You Fly</a></li>
                                        <li><a href="/html/extra-pages-inflight-experience.html">Inflight Experience</a></li>
                                        <li><a href="/html/extra-pages-things-todo1.html">Things To Do 1</a></li>
                                        <li><a href="/html/extra-pages-things-todo2.html">Things To Do 2</a></li>
                                        <li><a href="/html/extra-pages-travel-essentials.html">Travel Essentials</a></li>
                                        <li><a href="/html/extra-pages-travel-stories.html">Travel Stories</a></li>
                                        <li><a href="/html/extra-pages-travel-guide.html">Travel Guide</a></li>
                                        <li><a href="/html/extra-pages-travel-ideas.html">Travel Ideas</a></li>
                                        <li><a href="/html/extra-pages-travel-insurance.html">Travel Insurance</a></li>
                                        <li><a href="/html/extra-pages-group-booking.html">Group Bookings</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Special Pages</a>
                                    <ul>
                                        <li><a href="/html/pages-404-1.html">404 Page 1</a></li>
                                        <li><a href="/html/pages-404-2.html">404 Page 2</a></li>
                                        <li><a href="/html/pages-404-3.html">404 Page 3</a></li>
                                        <li><a href="/html/pages-coming-soon1.html">Coming Soon 1</a></li>
                                        <li><a href="/html/pages-coming-soon2.html">Coming Soon 2</a></li>
                                        <li><a href="/html/pages-coming-soon3.html">Coming Soon 3</a></li>
                                        <li><a href="/html/pages-loading1.html">Loading Page 1</a></li>
                                        <li><a href="/html/pages-loading2.html">Loading Page 2</a></li>
                                        <li><a href="/html/pages-loading3.html">Loading Page 3</a></li>
                                        <li><a href="/html/pages-login1.html">Login Page 1</a></li>
                                        <li><a href="/html/pages-login2.html">Login Page 2</a></li>
                                        <li><a href="/html/pages-login3.html">Login Page 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Shortcodes</a>
                            <ul>
                                <li><a href="/html/shortcode-accordions-toggles.html">Accordions &amp; Toggles</a></li>
                                <li><a href="/html/shortcode-tabs.html">Tabs</a></li>
                                <li><a href="/html/shortcode-buttons.html">Buttons</a></li>
                                <li><a href="/html/shortcode-icon-boxes.html">Icon Boxes</a></li>
                                <li><a href="/html/shortcode-gallery-styles.html">Image &amp; Gallery Styles</a></li>
                                <li><a href="/html/shortcode-image-box-styles.html">Image Box Styles</a></li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Listing Styles</a>
                                    <ul>
                                        <li><a href="/html/shortcode-listing-style1.html">Listing Style 01</a></li>
                                        <li><a href="/html/shortcode-listing-style2.html">Listing Style 02</a></li>
                                        <li><a href="/html/shortcode-listing-style3.html">Listing Style 03</a></li>
                                    </ul>
                                </li>
                                <li><a href="/html/shortcode-dropdowns.html">Dropdowns</a></li>
                                <li><a href="/html/shortcode-pricing-tables.html">Pricing Tables</a></li>
                                <li><a href="/html/shortcode-testimonials.html">Testimonials</a></li>
                                <li><a href="/html/shortcode-our-team.html">Our Team</a></li>
                                <li><a href="/html/shortcode-gallery-popup.html">Gallery Popup</a></li>
                                <li><a href="/html/shortcode-map-popup.html">Map Popup</a></li>
                                <li><a href="/html/shortcode-style-changer.html">Style Changer</a></li>
                                <li><a href="/html/shortcode-typography.html">Typography</a></li>
                                <li><a href="/html/shortcode-animations.html">Animations</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Bonus</a>
                            <ul>
                                <li><a href="/html/dashboard1.html">Dashboard 1</a></li>
                                <li><a href="/html/dashboard2.html">Dashboard 2</a></li>
                                <li><a href="/html/dashboard3.html">Dashboard 3</a></li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">7 Footer Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/footer-style1.html">Footer Style 1</a></li>
                                        <li><a href="/html/footer-style2.html">Footer Style 2</a></li>
                                        <li><a href="/html/footer-style3.html">Footer Style 3</a></li>
                                        <li><a href="/html/footer-style4.html">Footer Style 4</a></li>
                                        <li><a href="/html/footer-style5.html">Footer Style 5</a></li>
                                        <li><a href="/html/footer-style6.html">Footer Style 6</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">8 Header Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/header-style1.html">Header Style 1</a></li>
                                        <li><a href="/html/header-style2.html">Header Style 2</a></li>
                                        <li><a href="/html/header-style3.html">Header Style 3</a></li>
                                        <li><a href="/html/header-style4.html">Header Style 4</a></li>
                                        <li><a href="/html/header-style5.html">Header Style 5</a></li>
                                        <li><a href="/html/header-style6.html">Header Style 6</a></li>
                                        <li><a href="/html/header-style7.html">Header Style 7</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">7 Inner Start Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/inner-starts-style1.html">Inner Start Style 1</a></li>
                                        <li><a href="/html/inner-starts-style2.html">Inner Start Style 2</a></li>
                                        <li><a href="/html/inner-starts-style3.html">Inner Start Style 3</a></li>
                                        <li><a href="/html/inner-starts-style4.html">Inner Start Style 4</a></li>
                                        <li><a href="/html/inner-starts-style5.html">Inner Start Style 5</a></li>
                                        <li><a href="/html/inner-starts-style6.html">Inner Start Style 6</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">3 Search Styles</a>
                                    <ul>
                                        <li><a href="/html/search-style1.html">Search Style 1</a></li>
                                        <li><a href="/html/search-style2.html">Search Style 2</a></li>
                                        <li><a href="/html/search-style3.html">Search Style 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    
                    <ul class="mobile-topnav container">
                        <li><a href="/html/#">MY ACCOUNT</a></li>
                        <li class="ribbon language menu-color-skin">
                            <a href="/html/#" data-toggle="collapse">ENGLISH</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="Dansk">Dansk</a></li>
                                <li><a href="/html/#" title="Deutsch">Deutsch</a></li>
                                <li class="active"><a href="/html/#" title="English">English</a></li>
                                <li><a href="/html/#" title="Español">Español</a></li>
                                <li><a href="/html/#" title="Français">Français</a></li>
                                <li><a href="/html/#" title="Italiano">Italiano</a></li>
                                <li><a href="/html/#" title="Magyar">Magyar</a></li>
                                <li><a href="/html/#" title="Nederlands">Nederlands</a></li>
                                <li><a href="/html/#" title="Norsk">Norsk</a></li>
                                <li><a href="/html/#" title="Polski">Polski</a></li>
                                <li><a href="/html/#" title="Português">Português</a></li>
                                <li><a href="/html/#" title="Suomi">Suomi</a></li>
                                <li><a href="/html/#" title="Svenska">Svenska</a></li>
                            </ul>
                        </li>
                        <li><a href="/html/#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="/html/#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency menu-color-skin">
                            <a href="/html/#">USD</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="AUD">AUD</a></li>
                                <li><a href="/html/#" title="BRL">BRL</a></li>
                                <li class="active"><a href="/html/#" title="USD">USD</a></li>
                                <li><a href="/html/#" title="CAD">CAD</a></li>
                                <li><a href="/html/#" title="CHF">CHF</a></li>
                                <li><a href="/html/#" title="CNY">CNY</a></li>
                                <li><a href="/html/#" title="CZK">CZK</a></li>
                                <li><a href="/html/#" title="DKK">DKK</a></li>
                                <li><a href="/html/#" title="EUR">EUR</a></li>
                                <li><a href="/html/#" title="GBP">GBP</a></li>
                                <li><a href="/html/#" title="HKD">HKD</a></li>
                                <li><a href="/html/#" title="HUF">HUF</a></li>
                                <li><a href="/html/#" title="IDR">IDR</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                </nav>
            </div>
            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="/html/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="/html/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="/html/#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="/html/#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="/html/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="/html/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="/html/#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="/html/#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Hotel Detailed</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="/html/#">HOME</a></li>
                    <li class="active">Hotel Detailed</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">
                        <div class="tab-container style1" id="hotel-main-content">
                            <ul class="tabs">
                                <li class="active"><a data-toggle="tab" href="/html/#photos-tab">fotoğraflar</a></li>
                                <li><a data-toggle="tab" href="#steet-view-tab">Tanıtım Videosu</a></li>
                                <li><a data-toggle="tab" href="/html/#calendar-tab">Harita</a></li>
                                <li class="pull-right"><a class="button btn-small yellow-bg white-color" href="/html/#">TRAVEL GUIDE</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="photos-tab" class="tab-pane fade in active">
                                    <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                                        <ul class="slides">
                                               @foreach($photos as $photo)
                                            <li><img src="../images/{{$photo->name}}" alt="" width="900" height="500" /></li>
                                            
                                            @endforeach

                                            
                                        </ul>

                                    </div>
                                    <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                        <ul class="slides">
                                          @foreach($photos as $photo)
                                            <li><img src="../images/{{$photo->name}}" alt="" width="70" height="70" /></li>
                                            
                                            @endforeach
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div id="map-tab" class="tab-pane fade">
                                    
                                </div>

                                <div id="steet-view-tab" class="tab-pane fade" style="height: 500px;">
                                    <iframe width="900" height="500" src="https://www.youtube.com/embed/{{explode('=' ,$company->promo)[1]}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                                <div id="calendar-tab" class="tab-pane fade">
                                <div class="mapouter"><div class="gmap_canvas"><iframe width="800" height="550" id="gmap_canvas" src="https://maps.google.com/maps?q={{ str_replace(" ","%20",$company->name) }}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.pureblack.de/webdesign-bremen/"></a></div></div>
                                </div>
                            </div>
                        </div>
                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="/html/#hotel-description" data-toggle="tab">Bilgiler</a></li>
                                <li><a href="/html/#hotel-availability" data-toggle="tab">Markalar</a></li>
                                <li><a href="/html/#hotel-amenities" data-toggle="tab">Hizmetler</a></li>
                                <li><a href="/html/#hotel-reviews" data-toggle="tab">Yorumlar</a></li>
                                <li><a href="/html/#hotel-faqs" data-toggle="tab">Yol Tarifi</a></li>
                                <li><a href="/html/#hotel-things-todo" data-toggle="tab">Ürünler</a></li>
                                <li><a href="/html/#hotel-write-review" data-toggle="tab">Yorum yap</a></li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>
                                                <li><label>İşyeri Adı:</label>{{$company->name}}</li>
                                                <li><label>Telefon Numarası:</label>{{$company->phone1}}</li>
                                                <li><label>2.Telefon Numarası:</label>{{$company->phone2}}</li>
                                                <li><label>Fax:</label>{{$company->fax}}</li>
                                                <li><label>E-posta:</label>{{$company->email}}</li>
                                                <li><label>İnternet Sitesi:</label>{{$company->website}}</li>
                                                <li><label>Çalışma Alanı:</label>{{$company->fields}}</li>
                                                <li><label>Etiketler:</label>{{$company->tags}}</li>
                                          
                                            </ul>
                                        </div>
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                    <li>
                                                        <p class="description">Always enjoyed my stay with Hilton Hotel and Resorts, top class room service and rooms have great outside views and luxury assessories. Thanks for great experience.</p>
                                                        <div class="author clearfix">
                                                            <a href="/html/#"><img src="/html/http://placehold.it/270x270" alt="" width="74" height="74" /></a>
                                                            <h5 class="name">Jessica Brown<small>guest</small></h5>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <p class="description">Always enjoyed my stay with Hilton Hotel and Resorts, top class room service and rooms have great outside views and luxury assessories. Thanks for great experience.</p>
                                                        <div class="author clearfix">
                                                            <a href="/html/#"><img src="/html/http://placehold.it/270x270" alt="" width="74" height="74" /></a>
                                                            <h5 class="name">Lisa Kimberly<small>guest</small></h5>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <p class="description">Always enjoyed my stay with Hilton Hotel and Resorts, top class room service and rooms have great outside views and luxury assessories. Thanks for great experience.</p>
                                                        <div class="author clearfix">
                                                            <a href="/html/#"><img src="/html/http://placehold.it/270x270" alt="" width="74" height="74" /></a>
                                                            <h5 class="name">Jessica Brown<small>guest</small></h5>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="long-bilgiler">
                                        <h2>Hakkımızda</h2>
                                        <p>
                                           {{$company->description}}
<br /><br />
 <h2>Vizyonumuz</h2>
{{$company->vizyon}}<br /><br /> <h2>Misyonumuz</h2>
                                        </p>
{{$company->misyon}}                                    </div>
                                </div>
                                <div class="tab-pane fade" id="hotel-availability">
                                    <form>
                                    
                                    </form>
                                    <h2>Servisler</h2>
                                    <div class="room-list listing-style3 hotel">
                                        <article class="box">
                                            <figure class="col-sm-4 col-md-3">
                                                <a class="hover-effect popup-gallery" href="/html/ajax/slideshow-popup.html" title=""><img width="230" height="160" src="/html/http://placehold.it/230x160" alt=""></a>
                                            </figure>
                                            <div class="details col-xs-12 col-sm-8 col-md-9">
                                                <div>
                                                    <div>
                                                        <div class="box-title">
                                                            <h4 class="title">Standard Family Room</h4>
                                                            <dl class="description">
                                                                <dt>Max Guests:</dt>
                                                                <dd>3 persons</dd>
                                                            </dl>
                                                        </div>
                                                        <div class="amenities">
                                                            <i class="soap-icon-wifi circle"></i>
                                                            <i class="soap-icon-fitnessfacility circle"></i>
                                                            <i class="soap-icon-fork circle"></i>
                                                            <i class="soap-icon-television circle"></i>
                                                        </div>
                                                    </div>
                                                    <div class="price-section">
                                                        <span class="price"><small>PER/NIGHT</small>$121</span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                    <div class="action-section">
                                                        <a href="/html/hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="box">
                                            <figure class="col-sm-4 col-md-3">
                                                <a class="hover-effect popup-gallery" href="/html/ajax/slideshow-popup.html" title=""><img width="230" height="160" src="/html/http://placehold.it/230x160" alt=""></a>
                                            </figure>
                                            <div class="details col-xs-12 col-sm-8 col-md-9">
                                                <div>
                                                    <div>
                                                        <div class="box-title">
                                                            <h4 class="title">Superior Double Room</h4>
                                                            <dl class="description">
                                                                <dt>Max Guests:</dt>
                                                                <dd>5 persons</dd>
                                                            </dl>
                                                        </div>
                                                        <div class="amenities">
                                                            <i class="soap-icon-wifi circle"></i>
                                                            <i class="soap-icon-fitnessfacility circle"></i>
                                                            <i class="soap-icon-fork circle"></i>
                                                            <i class="soap-icon-television circle"></i>
                                                        </div>
                                                    </div>
                                                    <div class="price-section">
                                                        <span class="price"><small>PER/NIGHT</small>$241</span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                    <div class="action-section">
                                                        <a href="/html/hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="box">
                                            <figure class="col-sm-4 col-md-3">
                                                <a class="hover-effect popup-gallery" href="/html/ajax/slideshow-popup.html" title=""><img width="230" height="160" src="/html/http://placehold.it/230x160" alt=""></a>
                                            </figure>
                                            <div class="details col-xs-12 col-sm-8 col-md-9">
                                                <div>
                                                    <div>
                                                        <div class="box-title">
                                                            <h4 class="title">Deluxe Single Room</h4>
                                                            <dl class="description">
                                                                <dt>Max Guests:</dt>
                                                                <dd>4 persons</dd>
                                                            </dl>
                                                        </div>
                                                        <div class="amenities">
                                                            <i class="soap-icon-wifi circle"></i>
                                                            <i class="soap-icon-fitnessfacility circle"></i>
                                                            <i class="soap-icon-fork circle"></i>
                                                            <i class="soap-icon-television circle"></i>
                                                        </div>
                                                    </div>
                                                    <div class="price-section">
                                                        <span class="price"><small>PER/NIGHT</small>$137</span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                    <div class="action-section">
                                                        <a href="/html/hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article class="box">
                                            <figure class="col-sm-4 col-md-3">
                                                <a class="hover-effect popup-gallery" href="/html/ajax/slideshow-popup.html" title=""><img width="230" height="160" src="/html/http://placehold.it/230x160" alt=""></a>
                                            </figure>
                                            <div class="details col-xs-12 col-sm-8 col-md-9">
                                                <div>
                                                    <div>
                                                        <div class="box-title">
                                                            <h4 class="title">Single Bed Room</h4>
                                                            <dl class="description">
                                                                <dt>Max Guests:</dt>
                                                                <dd>2 persons</dd>
                                                            </dl>
                                                        </div>
                                                        <div class="amenities">
                                                            <i class="soap-icon-wifi circle"></i>
                                                            <i class="soap-icon-fitnessfacility circle"></i>
                                                            <i class="soap-icon-fork circle"></i>
                                                            <i class="soap-icon-television circle"></i>
                                                        </div>
                                                    </div>
                                                    <div class="price-section">
                                                        <span class="price"><small>PER/NIGHT</small>$55</span>
                                                    </div>
                                                </div>
                                                <div>
                                                    <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id, consectetur adipi deese cing elit maus fringilla bibe endum.</p>
                                                    <div class="action-section">
                                                        <a href="/html/hotel-booking.html" title="" class="button btn-small full-width text-center">BOOK NOW</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <a href="/html/#" class="load-more button full-width btn-large fourty-space">LOAD MORE ROOMS</a>
                                    </div>
                                    
                                </div>
                                <div class="tab-pane fade" id="hotel-amenities">
                                    <h2>Amenities Style 01</h2>
                                    
                                    <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                                    <ul class="amenities clearfix style1">
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-wifi"></i>WI_FI</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-swimming"></i>swimming pool</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-television"></i>television</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-coffee"></i>coffee</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-aircon"></i>air conditioning</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-fitnessfacility"></i>fitness facility</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-fridge"></i>fridge</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-winebar"></i>wine bar</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-smoking"></i>smoking allowed</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-entertainment"></i>entertainment</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-securevault"></i>secure vault</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-pickanddrop"></i>pick and drop</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-phone"></i>room service</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-pets"></i>pets allowed</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-playplace"></i>play place</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-breakfast"></i>complimentary breakfast</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-parking"></i>Free parking</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-conference"></i>conference room</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-fireplace"></i>fire place</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-handicapaccessiable"></i>Handicap Accessible</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-doorman"></i>Doorman</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-tub"></i>Hot Tub</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-elevator"></i>Elevator in Building</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style1"><i class="soap-icon-star"></i>Suitable for Events</div>
                                        </li>
                                    </ul>
                                    <br />
                                    
                                    <h2>Amenities Style 02</h2>
                                    <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                                    <ul class="amenities clearfix style2">
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-wifi circle"></i>WI_FI</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-swimming circle"></i>swimming pool</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-television circle"></i>television</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-coffee circle"></i>coffee</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-aircon circle"></i>air conditioning</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-fitnessfacility circle"></i>fitness facility</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-fridge circle"></i>fridge</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-winebar circle"></i>wine bar</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-smoking circle"></i>smoking allowed</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-entertainment circle"></i>entertainment</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-securevault circle"></i>secure vault</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-pickanddrop circle"></i>pick and drop</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-phone circle"></i>room service</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-pets circle"></i>pets allowed</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-playplace circle"></i>play place</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-breakfast circle"></i>complimentary breakfast</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-parking circle"></i>Free parking</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-conference circle"></i>conference room</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-fireplace circle"></i>fire place</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-handicapaccessiable circle"></i>Handicap Accessible</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-doorman circle"></i>Doorman</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-tub circle"></i>Hot Tub</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-elevator circle"></i>Elevator in Building</div>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <div class="icon-box style2"><i class="soap-icon-star circle"></i>Suitable for Events</div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="hotel-reviews">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="rating table-cell col-sm-4">
                                            <span class="score">3.9/5.0</span>
                                            <div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div>
                                            <a href="/html/#" class="goto-writereview-pane button green btn-small full-width">WRITE A REVIEW</a>
                                        </div>
                                        <div class="table-cell col-sm-8">
                                            <div class="detailed-rating">
                                                <ul class="clearfix">
                                                    <li class="col-md-6"><div class="each-rating"><label>service</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>Value</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>Sleep Quality</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>Cleanliness</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>location</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>rooms</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>swimming pool</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                    <li class="col-md-6"><div class="each-rating"><label>fitness facility</label><div class="five-stars-container"><div class="five-stars" style="width: 78%;"></div></div></div></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="guest-reviews">
                                        <h2>Guest Reviews</h2>
                                        <div class="guest-review table-wrapper">
                                            <div class="col-xs-3 col-md-2 author table-cell">
                                                <a href="/html/#"><img src="/html/http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                <p class="name">Jessica Brown</p>
                                                <p class="date">NOV, 12, 2013</p>
                                            </div>
                                            <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                <div class="comment-header clearfix">
                                                    <h4 class="comment-title">We had great experience while our stay and Hilton Hotels!</h4>
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div class="five-stars" style="width: 80%;"></div></div>
                                                        <span class="score">4.0/5.0</span>
                                                    </div>
                                                </div>
                                                <div class="comment-content">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="guest-review table-wrapper">
                                            <div class="col-xs-3 col-md-2 author table-cell">
                                                <a href="/html/#"><img src="/html/http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                <p class="name">David Jhon</p>
                                                <p class="date">NOV, 12, 2013</p>
                                            </div>
                                            <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                <div class="comment-header clearfix">
                                                    <h4 class="comment-title">I love the speediness of their services.</h4>
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div class="five-stars" style="width: 64%;"></div></div>
                                                        <span class="score">3.2/5.0</span>
                                                    </div>
                                                </div>
                                                <div class="comment-content">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="guest-review table-wrapper">
                                            <div class="col-xs-3 col-md-2 author table-cell">
                                                <a href="/html/#"><img src="/html/http://placehold.it/270x263" alt="" width="270" height="263" /></a>
                                                <p class="name">Kyle Martin</p>
                                                <p class="date">NOV, 12, 2013</p>
                                            </div>
                                            <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                <div class="comment-header clearfix">
                                                    <h4 class="comment-title">When you look outside from the windows its breath taking.</h4>
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div class="five-stars" style="width: 76%;"></div></div>
                                                        <span class="score">3.8/5.0</span>
                                                    </div>
                                                </div>
                                                <div class="comment-content">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's stand dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="/html/#" class="button full-width btn-large">LOAD MORE REVIEWS</a>
                                </div>
                                <div class="tab-pane fade" id="hotel-faqs">
                                    <h2>Frquently Asked Questions</h2>
                                    <div class="topics">
                                        <ul class="check-square clearfix">
                                            <li class="col-sm-6 col-md-4"><a href="/html/#">address &amp; map</a></li>
                                            <li class="col-sm-6 col-md-4"><a href="/html/#">messaging</a></li>
                                            <li class="col-sm-6 col-md-4"><a href="/html/#">refunds</a></li>
                                            <li class="col-sm-6 col-md-4"><a href="/html/#">pricing</a></li>
                                            <li class="col-sm-6 col-md-4 active"><a href="/html/#">reservation requests</a></li>
                                            <li class="col-sm-6 col-md-4"><a href="/html/#">your reservation</a></li>
                                        </ul>
                                    </div>
                                    <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                                    <div class="toggle-container">
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question1" data-toggle="collapse">How do I know a reservation is accepted or confirmed?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question1">
                                                <div class="panel-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question2" data-toggle="collapse">What do I do after I receive a reservation request from a guest?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question2">
                                                <div class="panel-content">
                                                    <p>Sed a justo enim. Vivamus volutpat ipsum ultrices augue porta lacinia. Proin in elementum enim. <span class="skin-color">Duis suscipit justo</span> non purus consequat molestie. Etiam pharetra ipsum sagittis sollicitudin ultricies. Praesent luctus, diam ut tempus aliquam, diam ante euismod risus, euismod viverra quam quam eget turpis. Nam <span class="skin-color">tristique congue</span> arcu, id bibendum diam. Ut hendrerit, leo a pellentesque porttitor, purus arcu tristique erat, in faucibus elit leo in turpis vitae luctus enim, a mollis nulla.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="" href="/html/#question3" data-toggle="collapse">How much time do I have to respond to a reservation request?</a>
                                            </h4>
                                            <div class="panel-collapse collapse in" id="question3">
                                                <div class="panel-content">
                                                    <p>Sed a justo enim. Vivamus volutpat ipsum ultrices augue porta lacinia. Proin in elementum enim. <span class="skin-color">Duis suscipit justo</span> non purus consequat molestie. Etiam pharetra ipsum sagittis sollicitudin ultricies. Praesent luctus, diam ut tempus aliquam, diam ante euismod risus, euismod viverra quam quam eget turpis. Nam <span class="skin-color">tristique congue</span> arcu, id bibendum diam. Ut hendrerit, leo a pellentesque porttitor, purus arcu tristique erat, in faucibus elit leo in turpis vitae luctus enim, a mollis nulla.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question4" data-toggle="collapse">Why can’t I call or email hotel or host before booking?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question4">
                                                <div class="panel-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question5" data-toggle="collapse">Am I allowed to decline reservation requests?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question5">
                                                <div class="panel-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question6" data-toggle="collapse">What happens if I let a reservation request expire?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question6">
                                                <div class="panel-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel style1 arrow-right">
                                            <h4 class="panel-title">
                                                <a class="collapsed" href="/html/#question7" data-toggle="collapse">How do I set reservation requirements?</a>
                                            </h4>
                                            <div class="panel-collapse collapse" id="question7">
                                                <div class="panel-content">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="hotel-things-todo">
                                    <h2>Things to Do</h2>
                                    <p>Maecenas vitae turpis condimentum metus tincidunt semper bibendum ut orci. Donec eget accumsan est. Duis laoreet sagittis elit et vehicula. Cras viverra posuere condimentum. Donec urna arcu, venenatis quis augue sit amet, mattis gravida nunc. Integer faucibus, tortor a tristique adipiscing, arcu metus luctus libero, nec vulputate risus elit id nibh.</p>
                                    <div class="activities image-box style2 innerstyle">
                                        <article class="box">
                                            <figure>
                                                <a title="" href="/html/#"><img width="250" height="161" alt="" src="/html/http://placehold.it/250x160"></a>
                                            </figure>
                                            <div class="details">
                                                <div class="details-header">
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                        <span class="reviews">25 reviews</span>
                                                    </div>
                                                    <h4 class="box-title">Central Park Walking Tour</h4>
                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                                <a class="button" title="" href="/html/#">MORE</a>
                                            </div>
                                        </article>
                                        <article class="box">
                                            <figure>
                                                <a title="" href="/html/#"><img width="250" height="161" alt="" src="/html/http://placehold.it/250x160"></a>
                                            </figure>
                                            <div class="details">
                                                <div class="details-header">
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                        <span class="reviews">25 reviews</span>
                                                    </div>
                                                    <h4 class="box-title">Museum of Modern Art</h4>
                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                                <a class="button" title="" href="/html/#">MORE</a>
                                            </div>
                                        </article>
                                        <article class="box">
                                            <figure>
                                                <a title="" href="/html/#"><img width="250" height="161" alt="" src="/html/http://placehold.it/250x160"></a>
                                            </figure>
                                            <div class="details">
                                                <div class="details-header">
                                                    <div class="review-score">
                                                        <div class="five-stars-container"><div style="width: 60%;" class="five-stars"></div></div>
                                                        <span class="reviews">25 reviews</span>
                                                    </div>
                                                    <h4 class="box-title">Crazy Horse Cabaret Show</h4>
                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim don't look even slightly believable.</p>
                                                <a class="button" title="" href="/html/#">MORE</a>
                                            </div>
                                        </article>
                                    </div>
                                </div>

<script>
$(function AjaxFunction(){

$('#buraya').on('click',function(e){
    $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
    })
    e.preventDefault(e);

        $.ajax({

        type:"POST",
        url:'/addcomment',
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
      
        },
        error: function(data){

        }
    })
    });
});

</script>

                                <div class="tab-pane fade" id="hotel-write-review">
                                    
                                    <form class="review-form" id="sign-up" method="post" action="/addcomment">
                                        {{ csrf_field()}}
                                        <form action="">
                                            <h4 class="title">Puanınız</h4>
  <input type="radio" name="rating" id="rating" value="1"> 1
  <input type="radio" name="rating" id="rating" value="2">2
  <input type="radio" name="rating" id="rating" value="3" checked="true"> 3
  <input type="radio" name="rating" id="rating" value="4"> 4
  <input type="radio" name="rating" id="rating" value="5"> 5
  
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title">Adınız</h4>
                                            <input id="author" type="text" name="author" class="input-text full-width" value="" placeholder="Ad Soyad" />
                                        </div>
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title">Email Adresiniz</h4>
                                            <input id="email" type="text" name="email" class="input-text full-width" value="" placeholder="E-posta" />
                                        </div>

                                        <div class="form-group">
                                            <h4 class="title">Yorumunuz</h4>
                                            <textarea id="content" name="content" class="input-text full-width" placeholder="Yorumunuzu yazınız" rows="5"></textarea>
                                        </div>
                                        <input type="hidden" id="company_id" value="{{$company->id}}" name="company_id">
                                       <!-- <input type="hidden" id="rating" value="unrated" name="rating"> -->
                                       <input type="hidden" id="status" value="pending" name="status">
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button onclick="sent()"  class="btn-large full-width">Yorumu Gönder</button>
                                        </div>
                                   
                                    </form>
                      
                      <div id="success" style="display: none; background-color: green; height: auto; border-radius: 24px;">
                                            <h3 style="background-color: white;">Yorumunuz Başarıyla gönderilmiştir. Admin Onayından sonra Yayınlanacaktır.</h3>

                                        </div>
                                        
                                </div>
                            </div>
                        
                       
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                            <figure>
                                <img width="114" height="85" src="../images/{{$photos->first()->name}}" alt="">
                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{$company->name}}<small><i class="soap-icon-departure yellow-color"></i><span class="fourty-space">{{ explode(" " ,$company->adress)[count(explode(" " ,$company->adress)) - 1]}}</span></small></h2>
                                <span class="price clearfix">
                                    <small class="pull-left">avg/night</small>
                                    <span class="pull-right">$620</span>
                                </span>
                                <div class="feedback clearfix">
                                    <div title="4 stars" class="five-stars-container" data-toggle="tooltip" data-placement="bottom"><span class="five-stars" style="width: 80%;"></span></div>
                                    <span class="review pull-right">270 yorum</span>
                                </div>
                                <p class="description">{{$company->description}}</p>
                                <a class="button yellow full-width uppercase btn-small">add to wishlist</a>
                            </div>
                        </article>
                        <div class="travelo-box contact-box">
                            <h4>Firmaya Ulaşın</h4>
                          
                            <address class="contact-details">
                            
                             <input type="text" class="input-text full-width" placeholder="Adınız" />
                          <input type="text" class="input-text full-width" placeholder="E-Posta" />
                          <textarea class="input-text full-width" placeholder="Mesaj"></textarea> 
                                <br>
                               <button>Mesajı Gönder</button>
                            </address>
                        </div>
                        <div class="travelo-box">
                            <h4>Benzer Firmalar</h4>
                            <div class="image-box style14">
                                <article class="box">
                                    <figure>
                                        <a href="/html/#"><img src="/html/http://placehold.it/63x59" alt="" /></a>
                                    </figure>
                                    <div class="details">
                                        <h5 class="box-title"><a href="/html/#">Plaza Tour Eiffel</a></h5>
                                        <label class="price-wrapper">
                                            <span class="price-per-unit">$170</span>avg/night
                                        </label>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure>
                                        <a href="/html/#"><img src="/html/http://placehold.it/63x59" alt="" /></a>
                                    </figure>
                                    <div class="details">
                                        <h5 class="box-title"><a href="/html/#">Sultan Gardens</a></h5>
                                        <label class="price-wrapper">
                                            <span class="price-per-unit">$620</span>avg/night
                                        </label>
                                    </div>
                                </article>
                                <article class="box">
                                    <figure>
                                        <a href="/html/#"><img src="/html/http://placehold.it/63x59" alt="" /></a>
                                    </figure>
                                    <div class="details">
                                        <h5 class="box-title"><a href="/html/#">Park Central</a></h5>
                                        <label class="price-wrapper">
                                            <span class="price-per-unit">$322</span>avg/night
                                        </label>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="travelo-box book-with-us-box">
                            <h4>Why Book with us?</h4>
                            <ul>
                                <li>
                                    <i class="soap-icon-hotel-1 circle"></i>
                                    <h5 class="title"><a href="/html/#">135,00+ Hotels</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-savings circle"></i>
                                    <h5 class="title"><a href="/html/#">Low Rates &amp; Savings</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                                <li>
                                    <i class="soap-icon-support circle"></i>
                                    <h5 class="title"><a href="/html/#">Excellent Support</a></h5>
                                    <p>Nunc cursus libero pur congue arut nimspnty.</p>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        
        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>Discover</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="/html/#">Safety</a></li>
                                <li class="col-xs-6"><a href="/html/#">About</a></li>
                                <li class="col-xs-6"><a href="/html/#">Travelo Picks</a></li>
                                <li class="col-xs-6"><a href="/html/#">Latest Jobs</a></li>
                                <li class="active col-xs-6"><a href="/html/#">Mobile</a></li>
                                <li class="col-xs-6"><a href="/html/#">Press Releases</a></li>
                                <li class="col-xs-6"><a href="/html/#">Why Host</a></li>
                                <li class="col-xs-6"><a href="/html/#">Blog Posts</a></li>
                                <li class="col-xs-6"><a href="/html/#">Social Connect</a></li>
                                <li class="col-xs-6"><a href="/html/#">Help Topics</a></li>
                                <li class="col-xs-6"><a href="/html/#">Site Map</a></li>
                                <li class="col-xs-6"><a href="/html/#">Policies</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Travel News</h2>
                            <ul class="travel-news">
                                <li>
                                    <div class="thumb">
                                        <a href="/html/#">
                                            <img src="/html/http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="/html/#">Amazing Places</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">25 Sep, 2013</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="/html/#">
                                            <img src="/html/http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="/html/#">Travel Insurance</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">24 Sep, 2013</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>
                            <br />
                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email" />
                            </div>
                            <br />
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>About Travelo</h2>
                            <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massaidp nequetiam lore elerisque.</p>
                            <br />
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br />
                                <a href="/html/#" class="contact-email">help@travelo.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="/html/#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="/html/#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="/html/#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="/html/#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="/html/#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="/html/#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="/html/#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="/html/index.html" title="Travelo - home">
                            <img src="/html/images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="/html/#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2014 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="/html/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="/html/js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/html/js/jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="/html/js/bootstrap.js"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="/html/components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/html/components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="/html/components/jquery.bxslider/jquery.bxslider.min.js"></script>
    
    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="/html/components/flexslider/jquery.flexslider-min.js"></script>
    
    <!-- Google Map Api -->
    <script src="/html/http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    
    <script type="text/javascript" src="/html/js/calendar.js"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="/html/js/jquery.stellar.min.js"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="/html/js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="/html/js/theme-scripts.js"></script>
     
    <script type="text/javascript">
        tjq(document).ready(function() {
            // calendar panel
            var cal = new Calendar();
            var unavailable_days = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            var price_arr = {3: '$170', 4: '$170', 5: '$170', 6: '$170', 7: '$170', 8: '$170', 9: '$170', 10: '$170', 11: '$170', 12: '$170', 13: '$170', 14: '$170', 15: '$170', 16: '$170', 17: '$170'};

            var current_date = new Date();
            var current_year_month = (1900 + current_date.getYear()) + "-" + (current_date.getMonth() + 1);
            tjq("#select-month").find("[value='" + current_year_month + "']").prop("selected", "selected");
            cal.generateHTML(current_date.getMonth(), (1900 + current_date.getYear()), unavailable_days, price_arr);
            tjq(".calendar").html(cal.getHTML());
            
            tjq("#select-month").change(function() {
                var selected_year_month = tjq("#select-month option:selected").val();
                var year = parseInt(selected_year_month.split("-")[0], 10);
                var month = parseInt(selected_year_month.split("-")[1], 10);
                cal.generateHTML(month - 1, year, unavailable_days, price_arr);
                tjq(".calendar").html(cal.getHTML());
            });
            
            
            tjq(".goto-writereview-pane").click(function(e) {
                e.preventDefault();
                tjq('#hotel-features .tabs a[href="/html/#hotel-write-review"]').tab('show')
            });
            
            // editable rating
            tjq(".editable-rating.five-stars-container").each(function() {
                var oringnal_value = tjq(this).data("original-stars");
                if (typeof oringnal_value == "undefined") {
                    oringnal_value = 0;
                } else {
                    //oringnal_value = 10 * parseInt(oringnal_value);
                }
                tjq(this).slider({
                    range: "min",
                    value: oringnal_value,
                    min: 0,
                    max: 5,
                    slide: function( event, ui ) {
                        
                    }
                });
            });
        });
        
        tjq('a[href="/html/#map-tab"]').on('shown.bs.tab', function (e) {
            var center = panorama.getPosition();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });
        tjq('a[href="/html/#steet-view-tab"]').on('shown.bs.tab', function (e) {
            fenway = panorama.getPosition();
            panoramaOptions.position = fenway;
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        });
        var map = null;
        var panorama = null;
        var fenway = new google.maps.LatLng(48.855702, 2.292577);
        var mapOptions = {
            center: fenway,
            zoom: 12
        };
        var panoramaOptions = {
            position: fenway,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        function initialize() {
            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
 <script>
                            
        $(function banabul(){

$('#sign-up').on('submit',function(e){
    $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
    })
    e.preventDefault(e);

        $.ajax({

        type:"POST",
        url:'/addcomment',
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
     
   },
        error: function(data){

        }
    })
    });
});


                        </script>
<script type="text/javascript">
    function sent() {
        // body...
        document.getElementById('success').style.display= "block";
    //     document.getElementById('author').value="";
    //     document.getElementById('email').value="";
    //     document.getElementById('content').value="";
    //

document.getElementById('sign-up').style.display="none";
     }
</script>

</body>
</html>

