<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Travelo - Travel, Tour Booking HTML5 Template</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/html/favicon.ico" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="/html/css/bootstrap.min.css">
    <link rel="stylesheet" href="/html/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/html/css/animate.min.css">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="/html/components/revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/html/components/flexslider/flexslider.css" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="/html/css/style.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="/html/css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="/html/css/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="/html/css/responsive.css">
    

    <style type="text/css">
        @import 'https://fonts.googleapis.com/css?family=Montserrat:400,700';
.aa-input-container {
  display: inline-block;
  position: relative; }
.aa-input-search {
  width: 300px;
  padding: 12px 28px 12px 12px;
  border: 2px solid #e4e4e4;
  border-radius: 4px;
  -webkit-transition: .2s;
  transition: .2s;
  font-family: "Montserrat", sans-serif;
  box-shadow: 4px 4px 0 rgba(241, 241, 241, 0.35);
  font-size: 11px;
  box-sizing: border-box;
  color: #333;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none; }
  .aa-input-search::-webkit-search-decoration, .aa-input-search::-webkit-search-cancel-button, .aa-input-search::-webkit-search-results-button, .aa-input-search::-webkit-search-results-decoration {
    display: none; }
  .aa-input-search:focus {
    outline: 0;
    border-color: #3a96cf;
    box-shadow: 4px 4px 0 rgba(58, 150, 207, 0.1); }
.aa-input-icon {
  height: 16px;
  width: 16px;
  position: absolute;
  top: 50%;
  right: 16px;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  fill: #e4e4e4; }
.aa-hint {
  color: #e4e4e4; }
.aa-dropdown-menu {
  background-color: #fff;
  border: 2px solid rgba(228, 228, 228, 0.6);
  border-top-width: 1px;
  font-family: "Montserrat", sans-serif;
  width: 300px;
  margin-top: 10px;
  box-shadow: 4px 4px 0 rgba(241, 241, 241, 0.35);
  font-size: 11px;
  border-radius: 4px;
  box-sizing: border-box; }
.aa-suggestion {
  padding: 12px;
  border-top: 1px solid rgba(228, 228, 228, 0.6);
  cursor: pointer;
  -webkit-transition: .2s;
  transition: .2s;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center; }
  .aa-suggestion:hover, .aa-suggestion.aa-cursor {
    background-color: rgba(241, 241, 241, 0.35); }
  .aa-suggestion > span:first-child {
    color: #333; }
  .aa-suggestion > span:last-child {
    text-transform: uppercase;
    color: #a9a9a9; }
.aa-suggestion > span:first-child em, .aa-suggestion > span:last-child em {
  font-weight: 700;
  font-style: normal;
  background-color: rgba(58, 150, 207, 0.1);
  padding: 2px 0 2px 2px; }
    </style>
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="/html/css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="/html/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="/html/http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <!-- Javascript Page Loader -->
    <script type="text/javascript" src="/html/js/pace.min.js" data-pace-options='{ "ajax": false }'></script>
    <script type="text/javascript" src="/html/js/page-loading.js"></script>
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-left">
                        <li><a href="/html/#">My Account</a></li>
                        <li class="ribbon">
                            <a href="/html/#">English</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="Dansk">Dansk</a></li>
                                <li><a href="/html/#" title="Deutsch">Deutsch</a></li>
                                <li class="active"><a href="/html/#" title="English">English</a></li>
                                <li><a href="/html/#" title="Español">Español</a></li>
                                <li><a href="/html/#" title="Français">Français</a></li>
                                <li><a href="/html/#" title="Italiano">Italiano</a></li>
                                <li><a href="/html/#" title="Magyar">Magyar</a></li>
                                <li><a href="/html/#" title="Nederlands">Nederlands</a></li>
                                <li><a href="/html/#" title="Norsk">Norsk</a></li>
                                <li><a href="/html/#" title="Polski">Polski</a></li>
                                <li><a href="/html/#" title="Português">Português</a></li>
                                <li><a href="/html/#" title="Suomi">Suomi</a></li>
                                <li><a href="/html/#" title="Svenska">Svenska</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="quick-menu pull-right">
                        <li><a href="/html/#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="/html/#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency">
                            <a href="/html/#" title="">USD</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="AUD">AUD</a></li>
                                <li><a href="/html/#" title="BRL">BRL</a></li>
                                <li class="active"><a href="/html/#" title="USD">USD</a></li>
                                <li><a href="/html/#" title="CAD">CAD</a></li>
                                <li><a href="/html/#" title="CHF">CHF</a></li>
                                <li><a href="/html/#" title="CNY">CNY</a></li>
                                <li><a href="/html/#" title="CZK">CZK</a></li>
                                <li><a href="/html/#" title="DKK">DKK</a></li>
                                <li><a href="/html/#" title="EUR">EUR</a></li>
                                <li><a href="/html/#" title="GBP">GBP</a></li>
                                <li><a href="/html/#" title="HKD">HKD</a></li>
                                <li><a href="/html/#" title="HUF">HUF</a></li>
                                <li><a href="/html/#" title="IDR">IDR</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="main-header">
                
                <a href="/html/#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="/html/index.html" title="Travelo - home">
                            <img src="/html/images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </h1>

                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="/html/index.html">Home</a>
                                <ul>
                                    <li><a href="/html/index.html">Home Layout 1</a></li>
                                    <li><a href="/html/homepage2.html">Home Layout 2</a></li>
                                    <li><a href="/html/homepage3.html">Home Layout 3</a></li>
                                    <li><a href="/html/homepage4.html">Home Layout 4</a></li>
                                    <li><a href="/html/homepage5.html">Home Layout 5</a></li>
                                    <li><a href="/html/homepage6.html">Home Layout 6</a></li>
                                    <li><a href="/html/homepage7.html">Home Layout 7</a></li>
                                    <li><a href="/html/homepage8.html">Home Layout 8</a></li>
                                    <li><a href="/html/homepage9.html">Home Layout 9</a></li>
                                    <li><a href="/html/homepage10.html">Home Layout 10</a></li>
                                    <li><a href="/html/homepage11.html">Home Layout 11</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/hotel-index.html">Hotels</a>
                                <ul>
                                    <li><a href="/html/hotel-index.html">Home Hotels</a></li>
                                    <li><a href="/html/hotel-list-view.html">List View</a></li>
                                    <li><a href="/html/hotel-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/hotel-block-view.html">Block View</a></li>
                                    <li><a href="/html/hotel-detailed.html">Detailed</a></li>
                                    <li><a href="/html/hotel-booking.html">Booking</a></li>
                                    <li><a href="/html/hotel-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/flight-index.html">Flights</a>
                                <ul>
                                    <li><a href="/html/flight-index.html">Home Flights</a></li>
                                    <li><a href="/html/flight-list-view.html">List View</a></li>
                                    <li><a href="/html/flight-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/flight-block-view.html">Block View</a></li>
                                    <li><a href="/html/flight-detailed.html">Detailed</a></li>
                                    <li><a href="/html/flight-booking.html">Booking</a></li>
                                    <li><a href="/html/flight-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/car-index.html">Cars</a>
                                <ul>
                                    <li><a href="/html/car-index.html">Home Cars</a></li>
                                    <li><a href="/html/car-list-view.html">List View</a></li>
                                    <li><a href="/html/car-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/car-block-view.html">Block View</a></li>
                                    <li><a href="/html/car-detailed.html">Detailed</a></li>
                                    <li><a href="/html/car-booking.html">Booking</a></li>
                                    <li><a href="/html/car-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/cruise-index.html">Cruises</a>
                                <ul>
                                    <li><a href="/html/cruise-index.html">Home Cruises</a></li>
                                    <li><a href="/html/cruise-list-view.html">List View</a></li>
                                    <li><a href="/html/cruise-grid-view.html">Grid View</a></li>
                                    <li><a href="/html/cruise-block-view.html">Block View</a></li>
                                    <li><a href="/html/cruise-detailed.html">Detailed</a></li>
                                    <li><a href="/html/cruise-booking.html">Booking</a></li>
                                    <li><a href="/html/cruise-thankyou.html">Thank You</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/tour-index.html">Tour</a>
                                <ul>
                                    <li><a href="/html/tour-index.html">Home Tour</a></li>
                                    <li><a href="/html/tour-fancy-package-2column.html">Fancy Packages 2 Column</a></li>
                                    <li><a href="/html/tour-fancy-package-3column.html">Fancy Packages 3 Column</a></li>
                                    <li><a href="/html/tour-fancy-package-4column.html">Fancy Packages 4 Column</a></li>
                                    <li><a href="/html/tour-simple-package-2column.html">Simple Packages 2 Column</a></li>
                                    <li><a href="/html/tour-simple-package-3column.html">Simple Packages 3 Column</a></li>
                                    <li><a href="/html/tour-simple-package-4column.html">Simple Packages 4 Column</a></li>
                                    <li><a href="/html/tour-simple-package-3column.html">Location - Eruope</a></li>
                                    <li><a href="/html/tour-simple-package-4column.html">Location - North America</a></li>
                                    <li><a href="/html/tour-detailed1.html">Detailed 1</a></li>
                                    <li><a href="/html/tour-detailed2.html">Detailed 2</a></li>
                                    <li><a href="/html/tour-booking.html">Booking</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children megamenu-menu">
                                <a href="/html/#">Pages</a>
                                <div class="megamenu-wrapper container" data-items-per-column="8">
                                    <div class="megamenu-holder">
                                        <ul class="megamenu">
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Standard Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/pages-aboutus1.html">About Us 1</a></li>
                                                    <li><a href="/html/pages-aboutus2.html">About Us 2</a></li>
                                                    <li><a href="/html/pages-services1.html">Services 1</a></li>
                                                    <li><a href="/html/pages-services2.html">Services 2</a></li>
                                                    <li><a href="/html/pages-photogallery-4column.html">Gallery 4 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-3column.html">Gallery 3 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-2column.html">Gallery 2 Column</a></li>
                                                    <li><a href="/html/pages-photogallery-fullview.html">Gallery Read</a></li>
                                                    <li><a href="/html/pages-blog-rightsidebar.html">Blog Right Sidebar</a></li>
                                                    <li><a href="/html/pages-blog-leftsidebar.html">Blog Left Sidebar</a></li>
                                                    <li><a href="/html/pages-blog-fullwidth.html">Blog Full Width</a></li>
                                                    <li><a href="/html/pages-blog-read.html">Blog Read</a></li>
                                                    <li><a href="/html/pages-faq1.html">Faq 1</a></li>
                                                    <li><a href="/html/pages-faq2.html">Faq 2</a></li>
                                                    <li><a href="/html/pages-layouts-leftsidebar.html">Layouts Left Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-rightsidebar.html">Layouts Right Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-twosidebar.html">Layouts Two Sidebar</a></li>
                                                    <li><a href="/html/pages-layouts-fullwidth.html">Layouts Full Width</a></li>
                                                    <li><a href="/html/pages-contactus1.html">Contact Us 1</a></li>
                                                    <li><a href="/html/pages-contactus2.html">Contact Us 2</a></li>
                                                    <li><a href="/html/pages-contactus3.html">Contact Us 3</a></li>
                                                    <li><a href="/html/pages-travelo-policies.html">Travelo Policies</a></li>
                                                    <li><a href="/html/pages-sitemap.html">Site Map</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Extra Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/extra-pages-holidays.html">Holidays</a></li>
                                                    <li><a href="/html/extra-pages-hotdeals.html">Hot Deals</a></li>
                                                    <li><a href="/html/extra-pages-before-you-fly.html">Before You Fly</a></li>
                                                    <li><a href="/html/extra-pages-inflight-experience.html">Inflight Experience</a></li>
                                                    <li><a href="/html/extra-pages-things-todo1.html">Things To Do 1</a></li>
                                                    <li><a href="/html/extra-pages-things-todo2.html">Things To Do 2</a></li>
                                                    <li><a href="/html/extra-pages-travel-essentials.html">Travel Essentials</a></li>
                                                    <li><a href="/html/extra-pages-travel-stories.html">Travel Stories</a></li>
                                                    <li><a href="/html/extra-pages-travel-guide.html">Travel Guide</a></li>
                                                    <li><a href="/html/extra-pages-travel-ideas.html">Travel Ideas</a></li>
                                                    <li><a href="/html/extra-pages-travel-insurance.html">Travel Insurance</a></li>
                                                    <li><a href="/html/extra-pages-group-booking.html">Group Bookings</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="/html/#">Special Pages</a>
                                                <ul class="clearfix">
                                                    <li><a href="/html/pages-404-1.html">404 Page 1</a></li>
                                                    <li><a href="/html/pages-404-2.html">404 Page 2</a></li>
                                                    <li><a href="/html/pages-404-3.html">404 Page 3</a></li>
                                                    <li><a href="/html/pages-coming-soon1.html">Coming Soon 1</a></li>
                                                    <li><a href="/html/pages-coming-soon2.html">Coming Soon 2</a></li>
                                                    <li><a href="/html/pages-coming-soon3.html">Coming Soon 3</a></li>
                                                    <li><a href="/html/pages-loading1.html">Loading Page 1</a></li>
                                                    <li><a href="/html/pages-loading2.html">Loading Page 2</a></li>
                                                    <li><a href="/html/pages-loading3.html">Loading Page 3</a></li>
                                                    <li><a href="/html/pages-login1.html">Login Page 1</a></li>
                                                    <li><a href="/html/pages-login2.html">Login Page 2</a></li>
                                                    <li><a href="/html/pages-login3.html">Login Page 3</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/#">Shortcodes</a>
                                <ul>
                                    <li><a href="/html/shortcode-accordions-toggles.html">Accordions &amp; Toggles</a></li>
                                    <li><a href="/html/shortcode-tabs.html">Tabs</a></li>
                                    <li><a href="/html/shortcode-buttons.html">Buttons</a></li>
                                    <li><a href="/html/shortcode-icon-boxes.html">Icon Boxes</a></li>
                                    <li><a href="/html/shortcode-gallery-styles.html">Image &amp; Gallery Styles</a></li>
                                    <li><a href="/html/shortcode-image-box-styles.html">Image Box Styles</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">Listing Styles</a>
                                        <ul>
                                            <li><a href="/html/shortcode-listing-style1.html">Listing Style 01</a></li>
                                            <li><a href="/html/shortcode-listing-style2.html">Listing Style 02</a></li>
                                            <li><a href="/html/shortcode-listing-style3.html">Listing Style 03</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/html/shortcode-dropdowns.html">Dropdowns</a></li>
                                    <li><a href="/html/shortcode-pricing-tables.html">Pricing Tables</a></li>
                                    <li><a href="/html/shortcode-testimonials.html">Testimonials</a></li>
                                    <li><a href="/html/shortcode-our-team.html">Our Team</a></li>
                                    <li><a href="/html/shortcode-gallery-popup.html">Gallery Popup</a></li>
                                    <li><a href="/html/shortcode-map-popup.html">Map Popup</a></li>
                                    <li><a href="/html/shortcode-style-changer.html">Style Changer</a></li>
                                    <li><a href="/html/shortcode-typography.html">Typography</a></li>
                                    <li><a href="/html/shortcode-animations.html">Animations</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/html/#">Bonus</a>
                                <ul>
                                    <li><a href="/html/dashboard1.html">Dashboard 1</a></li>
                                    <li><a href="/html/dashboard2.html">Dashboard 2</a></li>
                                    <li><a href="/html/dashboard3.html">Dashboard 3</a></li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">7 Footer Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/footer-style1.html">Footer Style 1</a></li>
                                            <li><a href="/html/footer-style2.html">Footer Style 2</a></li>
                                            <li><a href="/html/footer-style3.html">Footer Style 3</a></li>
                                            <li><a href="/html/footer-style4.html">Footer Style 4</a></li>
                                            <li><a href="/html/footer-style5.html">Footer Style 5</a></li>
                                            <li><a href="/html/footer-style6.html">Footer Style 6</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">8 Header Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/header-style1.html">Header Style 1</a></li>
                                            <li><a href="/html/header-style2.html">Header Style 2</a></li>
                                            <li><a href="/html/header-style3.html">Header Style 3</a></li>
                                            <li><a href="/html/header-style4.html">Header Style 4</a></li>
                                            <li><a href="/html/header-style5.html">Header Style 5</a></li>
                                            <li><a href="/html/header-style6.html">Header Style 6</a></li>
                                            <li><a href="/html/header-style7.html">Header Style 7</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">7 Inner Start Styles</a>
                                        <ul>
                                            <li><a href="/html/#">Default Style</a></li>
                                            <li><a href="/html/inner-starts-style1.html">Inner Start Style 1</a></li>
                                            <li><a href="/html/inner-starts-style2.html">Inner Start Style 2</a></li>
                                            <li><a href="/html/inner-starts-style3.html">Inner Start Style 3</a></li>
                                            <li><a href="/html/inner-starts-style4.html">Inner Start Style 4</a></li>
                                            <li><a href="/html/inner-starts-style5.html">Inner Start Style 5</a></li>
                                            <li><a href="/html/inner-starts-style6.html">Inner Start Style 6</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children">
                                        <a href="/html/#">3 Search Styles</a>
                                        <ul>
                                            <li><a href="/html/search-style1.html">Search Style 1</a></li>
                                            <li><a href="/html/search-style2.html">Search Style 2</a></li>
                                            <li><a href="/html/search-style3.html">Search Style 3</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="/html/index.html">Home</a>
                            <ul>
                                <li><a href="/html/index.html">Home Layout 1</a></li>
                                <li><a href="/html/homepage2.html">Home Layout 2</a></li>
                                <li><a href="/html/homepage3.html">Home Layout 3</a></li>
                                <li><a href="/html/homepage4.html">Home Layout 4</a></li>
                                <li><a href="/html/homepage5.html">Home Layout 5</a></li>
                                <li><a href="/html/homepage6.html">Home Layout 6</a></li>
                                <li><a href="/html/homepage7.html">Home Layout 7</a></li>
                                <li><a href="/html/homepage8.html">Home Layout 8</a></li>
                                <li><a href="/html/homepage9.html">Home Layout 9</a></li>
                                <li><a href="/html/homepage10.html">Home Layout 10</a></li>
                                <li><a href="/html/homepage11.html">Home Layout 11</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/hotel-index.html">Hotels</a>
                            <ul>
                                <li><a href="/html/hotel-index.html">Home Hotels</a></li>
                                <li><a href="/html/hotel-list-view.html">List View</a></li>
                                <li><a href="/html/hotel-grid-view.html">Grid View</a></li>
                                <li><a href="/html/hotel-block-view.html">Block View</a></li>
                                <li><a href="/html/hotel-detailed.html">Detailed</a></li>
                                <li><a href="/html/hotel-booking.html">Booking</a></li>
                                <li><a href="/html/hotel-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/flight-index.html">Flights</a>
                            <ul>
                                <li><a href="/html/flight-index.html">Home Flights</a></li>
                                <li><a href="/html/flight-list-view.html">List View</a></li>
                                <li><a href="/html/flight-grid-view.html">Grid View</a></li>
                                <li><a href="/html/flight-block-view.html">Block View</a></li>
                                <li><a href="/html/flight-detailed.html">Detailed</a></li>
                                <li><a href="/html/flight-booking.html">Booking</a></li>
                                <li><a href="/html/flight-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/car-index.html">Cars</a>
                            <ul>
                                <li><a href="/html/car-index.html">Home Cars</a></li>
                                <li><a href="/html/car-list-view.html">List View</a></li>
                                <li><a href="/html/car-grid-view.html">Grid View</a></li>
                                <li><a href="/html/car-block-view.html">Block View</a></li>
                                <li><a href="/html/car-detailed.html">Detailed</a></li>
                                <li><a href="/html/car-booking.html">Booking</a></li>
                                <li><a href="/html/car-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/cruise-index.html">Cruises</a>
                            <ul>
                                <li><a href="/html/cruise-index.html">Home Cruises</a></li>
                                <li><a href="/html/cruise-list-view.html">List View</a></li>
                                <li><a href="/html/cruise-grid-view.html">Grid View</a></li>
                                <li><a href="/html/cruise-block-view.html">Block View</a></li>
                                <li><a href="/html/cruise-detailed.html">Detailed</a></li>
                                <li><a href="/html/cruise-booking.html">Booking</a></li>
                                <li><a href="/html/cruise-thankyou.html">Thank You</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/tour-index.html">Tour</a>
                            <ul>
                                <li><a href="/html/tour-index.html">Home Tour</a></li>
                                <li><a href="/html/tour-fancy-package-2column.html">Fancy Packages 2 Column</a></li>
                                <li><a href="/html/tour-fancy-package-3column.html">Fancy Packages 3 Column</a></li>
                                <li><a href="/html/tour-fancy-package-4column.html">Fancy Packages 4 Column</a></li>
                                <li><a href="/html/tour-simple-package-2column.html">Simple Packages 2 Column</a></li>
                                <li><a href="/html/tour-simple-package-3column.html">Simple Packages 3 Column</a></li>
                                <li><a href="/html/tour-simple-package-4column.html">Simple Packages 4 Column</a></li>
                                <li><a href="/html/tour-simple-package-3column.html">Location - Eruope</a></li>
                                <li><a href="/html/tour-simple-package-4column.html">Location - North America</a></li>
                                <li><a href="/html/tour-detailed1.html">Detailed 1</a></li>
                                <li><a href="/html/tour-detailed2.html">Detailed 2</a></li>
                                <li><a href="/html/tour-booking.html">Booking</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Pages</a>
                            <ul>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Standard Pages</a>
                                    <ul>
                                        <li><a href="/html/pages-aboutus1.html">About Us 1</a></li>
                                        <li><a href="/html/pages-aboutus2.html">About Us 2</a></li>
                                        <li><a href="/html/pages-services1.html">Services 1</a></li>
                                        <li><a href="/html/pages-services2.html">Services 2</a></li>
                                        <li><a href="/html/pages-photogallery-4column.html">Gallery 4 Column</a></li>
                                        <li><a href="/html/pages-photogallery-3column.html">Gallery 3 Column</a></li>
                                        <li><a href="/html/pages-photogallery-2column.html">Gallery 2 Column</a></li>
                                        <li><a href="/html/pages-photogallery-fullview.html">Gallery Read</a></li>
                                        <li><a href="/html/pages-blog-rightsidebar.html">Blog Right Sidebar</a></li>
                                        <li><a href="/html/pages-blog-leftsidebar.html">Blog Left Sidebar</a></li>
                                        <li><a href="/html/pages-blog-fullwidth.html">Blog Full Width</a></li>
                                        <li><a href="/html/pages-blog-read.html">Blog Read</a></li>
                                        <li><a href="/html/pages-faq1.html">Faq 1</a></li>
                                        <li><a href="/html/pages-faq2.html">Faq 2</a></li>
                                        <li><a href="/html/pages-layouts-leftsidebar.html">Layouts Left Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-rightsidebar.html">Layouts Right Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-twosidebar.html">Layouts Two Sidebar</a></li>
                                        <li><a href="/html/pages-layouts-fullwidth.html">Layouts Full Width</a></li>
                                        <li><a href="/html/pages-contactus1.html">Contact Us 1</a></li>
                                        <li><a href="/html/pages-contactus2.html">Contact Us 2</a></li>
                                        <li><a href="/html/pages-contactus3.html">Contact Us 3</a></li>
                                        <li><a href="/html/pages-travelo-policies.html">Travelo Policies</a></li>
                                        <li><a href="/html/pages-sitemap.html">Site Map</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Extra Pages</a>
                                    <ul>
                                        <li><a href="/html/extra-pages-holidays.html">Holidays</a></li>
                                        <li><a href="/html/extra-pages-hotdeals.html">Hot Deals</a></li>
                                        <li><a href="/html/extra-pages-before-you-fly.html">Before You Fly</a></li>
                                        <li><a href="/html/extra-pages-inflight-experience.html">Inflight Experience</a></li>
                                        <li><a href="/html/extra-pages-things-todo1.html">Things To Do 1</a></li>
                                        <li><a href="/html/extra-pages-things-todo2.html">Things To Do 2</a></li>
                                        <li><a href="/html/extra-pages-travel-essentials.html">Travel Essentials</a></li>
                                        <li><a href="/html/extra-pages-travel-stories.html">Travel Stories</a></li>
                                        <li><a href="/html/extra-pages-travel-guide.html">Travel Guide</a></li>
                                        <li><a href="/html/extra-pages-travel-ideas.html">Travel Ideas</a></li>
                                        <li><a href="/html/extra-pages-travel-insurance.html">Travel Insurance</a></li>
                                        <li><a href="/html/extra-pages-group-booking.html">Group Bookings</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Special Pages</a>
                                    <ul>
                                        <li><a href="/html/pages-404-1.html">404 Page 1</a></li>
                                        <li><a href="/html/pages-404-2.html">404 Page 2</a></li>
                                        <li><a href="/html/pages-404-3.html">404 Page 3</a></li>
                                        <li><a href="/html/pages-coming-soon1.html">Coming Soon 1</a></li>
                                        <li><a href="/html/pages-coming-soon2.html">Coming Soon 2</a></li>
                                        <li><a href="/html/pages-coming-soon3.html">Coming Soon 3</a></li>
                                        <li><a href="/html/pages-loading1.html">Loading Page 1</a></li>
                                        <li><a href="/html/pages-loading2.html">Loading Page 2</a></li>
                                        <li><a href="/html/pages-loading3.html">Loading Page 3</a></li>
                                        <li><a href="/html/pages-login1.html">Login Page 1</a></li>
                                        <li><a href="/html/pages-login2.html">Login Page 2</a></li>
                                        <li><a href="/html/pages-login3.html">Login Page 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Shortcodes</a>
                            <ul>
                                <li><a href="/html/shortcode-accordions-toggles.html">Accordions &amp; Toggles</a></li>
                                <li><a href="/html/shortcode-tabs.html">Tabs</a></li>
                                <li><a href="/html/shortcode-buttons.html">Buttons</a></li>
                                <li><a href="/html/shortcode-icon-boxes.html">Icon Boxes</a></li>
                                <li><a href="/html/shortcode-gallery-styles.html">Image &amp; Gallery Styles</a></li>
                                <li><a href="/html/shortcode-image-box-styles.html">Image Box Styles</a></li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">Listing Styles</a>
                                    <ul>
                                        <li><a href="/html/shortcode-listing-style1.html">Listing Style 01</a></li>
                                        <li><a href="/html/shortcode-listing-style2.html">Listing Style 02</a></li>
                                        <li><a href="/html/shortcode-listing-style3.html">Listing Style 03</a></li>
                                    </ul>
                                </li>
                                <li><a href="/html/shortcode-dropdowns.html">Dropdowns</a></li>
                                <li><a href="/html/shortcode-pricing-tables.html">Pricing Tables</a></li>
                                <li><a href="/html/shortcode-testimonials.html">Testimonials</a></li>
                                <li><a href="/html/shortcode-our-team.html">Our Team</a></li>
                                <li><a href="/html/shortcode-gallery-popup.html">Gallery Popup</a></li>
                                <li><a href="/html/shortcode-map-popup.html">Map Popup</a></li>
                                <li><a href="/html/shortcode-style-changer.html">Style Changer</a></li>
                                <li><a href="/html/shortcode-typography.html">Typography</a></li>
                                <li><a href="/html/shortcode-animations.html">Animations</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="/html/#">Bonus</a>
                            <ul>
                                <li><a href="/html/dashboard1.html">Dashboard 1</a></li>
                                <li><a href="/html/dashboard2.html">Dashboard 2</a></li>
                                <li><a href="/html/dashboard3.html">Dashboard 3</a></li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">7 Footer Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/footer-style1.html">Footer Style 1</a></li>
                                        <li><a href="/html/footer-style2.html">Footer Style 2</a></li>
                                        <li><a href="/html/footer-style3.html">Footer Style 3</a></li>
                                        <li><a href="/html/footer-style4.html">Footer Style 4</a></li>
                                        <li><a href="/html/footer-style5.html">Footer Style 5</a></li>
                                        <li><a href="/html/footer-style6.html">Footer Style 6</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">8 Header Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/header-style1.html">Header Style 1</a></li>
                                        <li><a href="/html/header-style2.html">Header Style 2</a></li>
                                        <li><a href="/html/header-style3.html">Header Style 3</a></li>
                                        <li><a href="/html/header-style4.html">Header Style 4</a></li>
                                        <li><a href="/html/header-style5.html">Header Style 5</a></li>
                                        <li><a href="/html/header-style6.html">Header Style 6</a></li>
                                        <li><a href="/html/header-style7.html">Header Style 7</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">7 Inner Start Styles</a>
                                    <ul>
                                        <li><a href="/html/#">Default Style</a></li>
                                        <li><a href="/html/inner-starts-style1.html">Inner Start Style 1</a></li>
                                        <li><a href="/html/inner-starts-style2.html">Inner Start Style 2</a></li>
                                        <li><a href="/html/inner-starts-style3.html">Inner Start Style 3</a></li>
                                        <li><a href="/html/inner-starts-style4.html">Inner Start Style 4</a></li>
                                        <li><a href="/html/inner-starts-style5.html">Inner Start Style 5</a></li>
                                        <li><a href="/html/inner-starts-style6.html">Inner Start Style 6</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/html/#">3 Search Styles</a>
                                    <ul>
                                        <li><a href="/html/search-style1.html">Search Style 1</a></li>
                                        <li><a href="/html/search-style2.html">Search Style 2</a></li>
                                        <li><a href="/html/search-style3.html">Search Style 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    
                    <ul class="mobile-topnav container">
                        <li><a href="/html/#">MY ACCOUNT</a></li>
                        <li class="ribbon language menu-color-skin">
                            <a href="/html/#" data-toggle="collapse">ENGLISH</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="Dansk">Dansk</a></li>
                                <li><a href="/html/#" title="Deutsch">Deutsch</a></li>
                                <li class="active"><a href="/html/#" title="English">English</a></li>
                                <li><a href="/html/#" title="Español">Español</a></li>
                                <li><a href="/html/#" title="Français">Français</a></li>
                                <li><a href="/html/#" title="Italiano">Italiano</a></li>
                                <li><a href="/html/#" title="Magyar">Magyar</a></li>
                                <li><a href="/html/#" title="Nederlands">Nederlands</a></li>
                                <li><a href="/html/#" title="Norsk">Norsk</a></li>
                                <li><a href="/html/#" title="Polski">Polski</a></li>
                                <li><a href="/html/#" title="Português">Português</a></li>
                                <li><a href="/html/#" title="Suomi">Suomi</a></li>
                                <li><a href="/html/#" title="Svenska">Svenska</a></li>
                            </ul>
                        </li>
                        <li><a href="/html/#travelo-login" class="soap-popupbox">LOGIN</a></li>
                        <li><a href="/html/#travelo-signup" class="soap-popupbox">SIGNUP</a></li>
                        <li class="ribbon currency menu-color-skin">
                            <a href="/html/#">USD</a>
                            <ul class="menu mini">
                                <li><a href="/html/#" title="AUD">AUD</a></li>
                                <li><a href="/html/#" title="BRL">BRL</a></li>
                                <li class="active"><a href="/html/#" title="USD">USD</a></li>
                                <li><a href="/html/#" title="CAD">CAD</a></li>
                                <li><a href="/html/#" title="CHF">CHF</a></li>
                                <li><a href="/html/#" title="CNY">CNY</a></li>
                                <li><a href="/html/#" title="CZK">CZK</a></li>
                                <li><a href="/html/#" title="DKK">DKK</a></li>
                                <li><a href="/html/#" title="EUR">EUR</a></li>
                                <li><a href="/html/#" title="GBP">GBP</a></li>
                                <li><a href="/html/#" title="HKD">HKD</a></li>
                                <li><a href="/html/#" title="HUF">HUF</a></li>
                                <li><a href="/html/#" title="IDR">IDR</a></li>
                            </ul>
                        </li>
                    </ul>
                    
                </nav>
            </div>
            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="/html/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="/html/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="/html/#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="/html/#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="/html/#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="/html/#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="/html/#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="/html/#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>
    
        <section id="content">
            <div class="search-box-wrapper">
                <div class="search-box container">
                 
              
                    
                    <div class="search-tab-content">
                        <div class="tab-pane fade active in" id="hotels-tab">
                            <form action="search" method="post">
                                 {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-sm-6 col-md-3">
                                        <h4 class="title">Where</h4>
                                        <label>Your Destination</label>
                                       <input name="keyword" type="find" id="aa-search-input" class="aa-input-search" placeholder="Search" >
                                     
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button type="submit" class="full-width icon-check animated" data-animation-type="bounce" data-animation-duration="1">Hemen Bul</button>
                                    
                                    </div>
                               
                                  
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="flights-tab">
                            <form action="flight-list-view.html" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group">
                                            <label>Leaving From</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                        <div class="form-group">
                                            <label>Going To</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">When</h4>
                                        <label>Departing On</label>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <div class="datepicker-wrap">
                                                    <input type="text" name="date_from" class="input-text full-width" placeholder="mm/dd/yy" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">anytime</option>
                                                        <option value="2">morning</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Arriving On</label>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <div class="datepicker-wrap">
                                                    <input type="text" name="date_to" class="input-text full-width" placeholder="mm/dd/yy" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">anytime</option>
                                                        <option value="2">morning</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                                <label>Adults</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <label>Kids</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Promo Code</label>
                                                <input type="text" class="input-text full-width" placeholder="type here" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                                <label>Infants</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 pull-right">
                                                <label>&nbsp;</label>
                                                <button class="full-width icon-check">SERACH NOW</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="flight-and-hotel-tab">
                            <form action="flight-list-view.html" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group">
                                            <label>Leaving From</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                        <div class="form-group">
                                            <label>Going To</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">When</h4>
                                        <label>Departing On</label>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <div class="datepicker-wrap">
                                                    <input type="text" name="date_from" class="input-text full-width" placeholder="mm/dd/yy" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">anytime</option>
                                                        <option value="2">morning</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <label>Arriving On</label>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <div class="datepicker-wrap">
                                                    <input type="text" name="date_to" class="input-text full-width" placeholder="mm/dd/yy" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">anytime</option>
                                                        <option value="2">morning</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                                <label>Adults</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <label>Kids</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Promo Code</label>
                                                <input type="text" class="input-text full-width" placeholder="type here" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                                <label>Rooms</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 pull-right">
                                                <label>&nbsp;</label>
                                                <button class="full-width icon-check">SERACH NOW</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="cars-tab">
                            <form action="car-list-view.html" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group">
                                            <label>Pick Up</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                        <div class="form-group">
                                            <label>Drop Off</label>
                                            <input type="text" class="input-text full-width" placeholder="city, distirct or specific airpot" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">When</h4>
                                        <div class="form-group">
                                            <label>Pick-Up Date / Time</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="datepicker-wrap">
                                                        <input type="text" name="date_from" class="input-text full-width" placeholder="mm/dd/yy" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="1">anytime</option>
                                                            <option value="2">morning</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Drop-Off Date / Time</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="datepicker-wrap">
                                                        <input type="text" name="date_to" class="input-text full-width" placeholder="mm/dd/yy" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="1">anytime</option>
                                                            <option value="2">morning</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-3">
                                                <label>Adults</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                                <label>Kids</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                        <option value="4">04</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Promo Code</label>
                                                <input type="text" class="input-text full-width" placeholder="type here" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Car Type</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="">select a car type</option>
                                                        <option value="economy">Economy</option>
                                                        <option value="compact">Compact</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>&nbsp;</label>
                                                <button class="full-width icon-check">SERACH NOW</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="cruises-tab">
                            <form action="cruise-list-view.html" method="post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group">
                                            <label>Your Destination</label>
                                            <input type="text" class="input-text full-width" placeholder="enter a destination or hotel name" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">When</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Departure Date</label>
                                                <div class="datepicker-wrap">
                                                    <input type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Cruise Length</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="">select length</option>
                                                        <option value="1">1-2 Nights</option>
                                                        <option value="2">3-4 Nights</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Cruise Line</label>
                                                <div class="selector">
                                                    <select class="full-width">
                                                        <option value="">select cruise line</option>
                                                        <option>Azamara Club Cruises</option>
                                                        <option>Carnival Cruise Lines</option>
                                                        <option>Celebrity Cruises</option>
                                                        <option>Costa Cruise Lines</option>
                                                        <option>Cruise &amp; Maritime Voyages</option>
                                                        <option>Crystal Cruises</option>
                                                        <option>Cunard Line Ltd.</option>
                                                        <option>Disney Cruise Line</option>
                                                        <option>Holland America Line</option>
                                                        <option>Hurtigruten Cruise Line</option>
                                                        <option>MSC Cruises</option>
                                                        <option>Norwegian Cruise Line</option>
                                                        <option>Oceania Cruises</option>
                                                        <option>Orion Expedition Cruises</option>
                                                        <option>P&amp;O Cruises</option>
                                                        <option>Paul Gauguin Cruises</option>
                                                        <option>Peter Deilmann Cruises</option>
                                                        <option>Princess Cruises</option>
                                                        <option>Regent Seven Seas Cruises</option>
                                                        <option>Royal Caribbean International</option>
                                                        <option>Seabourn Cruise Line</option>
                                                        <option>Silversea Cruises</option>
                                                        <option>Star Clippers</option>
                                                        <option>Swan Hellenic Cruises</option>
                                                        <option>Thomson Cruises</option>
                                                        <option>Viking River Cruises</option>
                                                        <option>Windstar Cruises</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>&nbsp;</label>
                                                <button class="icon-check full-width">SEARCH NOW</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="flight-status-tab">
                            <form action="flight-list-view.html" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Leaving From</label>
                                                <input type="text" class="input-text full-width" placeholder="enter a city or place name" />
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Going To</label>
                                                <input type="text" class="input-text full-width" placeholder="enter a city or place name" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <h4 class="title">When</h4>
                                        <div class="form-group">
                                            <label>Departure Date</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group">
                                            <label>Flight Number</label>
                                            <input type="text" class="input-text full-width" placeholder="enter flight number" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 fixheight">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button class="icon-check full-width">SEARCH NOW</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="tab-pane fade" id="online-checkin-tab">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4 class="title">Where</h4>
                                        <div class="form-group row">
                                            <div class="col-xs-6">
                                                <label>Leaving From</label>
                                                <input type="text" class="input-text full-width" placeholder="enter a city or place name" />
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Going To</label>
                                                <input type="text" class="input-text full-width" placeholder="enter a city or place name" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <h4 class="title">When</h4>
                                        <div class="form-group">
                                            <label>Departure Date</label>
                                            <div class="datepicker-wrap">
                                                <input type="text" class="input-text full-width" placeholder="mm/dd/yy" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-md-2">
                                        <h4 class="title">Who</h4>
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input type="text" class="input-text full-width" placeholder="enter your full name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2 fixheight">
                                        <label class="hidden-xs">&nbsp;</label>
                                        <button class="icon-check full-width">SEARCH NOW</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Popuplar Destinations -->
            <div class="destinations section">
                <div class="container">
                    <h2>Popular Destinations</h2>
                    <div class="row image-box style1 add-clearfix">
                        @foreach($companies as $company)
                        <div class="col-sms-6 col-sm-6 col-md-3">
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1">
                                    <a href="/html/ajax/slideshow-popup.html" title="" class="hover-effect popup-gallery"><img src="/html/http://placehold.it/270x160" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>{{ explode(" " ,$company->adress)[count(explode(" " ,$company->adress)) - 1]}} </small></span>
                                    <h4 class="box-title"><a href="firma/{{$company->id}}">{{$company->name}}<small>{{$company->fields}}</small></a></h4>
                                </div>
                            </article>
                        </div>
                        @endforeach
                        <div class="col-sms-6 col-sm-6 col-md-3">
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1" data-animation-delay="0.3">
                                    <a href="/html/ajax/slideshow-popup.html" title="" class="hover-effect popup-gallery"><img src="/html/http://placehold.it/270x160" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$170</span>
                                    <h4 class="box-title"><a href="/html/hotel-detailed.html">Hilton Hotel<small>LONDON</small></a></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sms-6 col-sm-6 col-md-3">
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1" data-animation-delay="0.6">
                                    <a href="/html/ajax/slideshow-popup.html" title="" class="hover-effect popup-gallery"><img src="/html/http://placehold.it/270x160" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$130</span>
                                    <h4 class="box-title"><a href="/html/hotel-detailed.html">MGM Grand<small>LAS VEGAS</small></a></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sms-6 col-sm-6 col-md-3">
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="1" data-animation-delay="0.9">
                                    <a href="/html/ajax/slideshow-popup.html" title="" class="hover-effect popup-gallery"><img src="/html/http://placehold.it/270x160" alt="" width="270" height="160" /></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$290</span>
                                    <h4 class="box-title"><a href="/html/hotel-detailed.html">Crown Casino<small>ASUTRALIA</small></a></h4>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Honeymoon -->
            <div class="honeymoon section global-map-area promo-box parallax" data-stellar-background-ratio="0.5">
                <div class="container">
                    <div class="col-sm-6 content-section description pull-right">
                        <h1 class="title">Popular Destinations for Honeymoon</h1>
                        <p>Nunc cursus libero purusac congue arcu cursus utsed vitae pulvinar massa idporta neque purusac Etiam elerisque mi id faucibus iaculis vitae pulvinar.</p>
                        <div class="row places image-box style9">
                             @foreach($companies as $company)
                            <div class="col-sms-4 col-sm-4">
                                <article class="box">
                                    <figure>
                                        <a href="/html/hotel-list-view.html" title="" class="hover-effect yellow middle-block animated" data-animation-type="fadeInUp" data-animation-duration="1">
                                            <img src="images/{{$company->photos->first()->name}}" alt="" /></a>
                                    </figure>
                                    <div class="details">
                                        <h4 class="box-title">{{$company->name}}<small>(990 PLACES)</small></h4>
                                        <a href="/html/hotel-list-view.html" title="" class="button">SEE ALL</a>
                                    </div>
                                </article>
                            </div>
                            @endforeach
                           
                        </div>
                    </div>
                    <div class="col-sm-6 image-container no-margin">
                        <img src="/html/http://placehold.it/524x540" alt="" class="animated" data-animation-type="fadeInUp" data-animation-duration="2">
                    </div>
                </div>
            </div>
            
            <!-- Did you Know? section -->
            <div class="offers section">
                <div class="container">
                    <h1 class="text-center">Did you know?</h1>
                    <p class="col-xs-9 center-block no-float text-center">Mauris ullamcorper nibh quis leo ultrices in hendrerit velit tristiqueut augue in nulla lacinia bibendum liberoras rutrum ac purus ut tristique.
Nullam placerat lacinia dolor quis pretium. Phasellus vitae lacinia quam, at pellentesque lorem. Sed euismod turpis quis mattis fringilla.</p>
                    <div class="row image-box style2">
                         @foreach($companies as $company)
                        <div class="col-md-6">
                            
                            
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInLeft" data-animation-duration="1">
                                    <a href="/html/#" title=""><img src="images/{{$company->photos->first()->name}}" alt="" width="270" height="192" /></a>
                                </figure>
                                <div class="details">
                                    <h4>{{$company->name}}</h4>
                                    <p>{{$company->description}}</p>
                                    <a href="/html/#" title="" class="button">SEE ALL</a>
                                </div>
                            </article>
                          
                        </div>
                          @endforeach
                        
                        <div class="col-md-6">
                            <article class="box">
                                <figure class="animated" data-animation-type="fadeInLeft" data-animation-duration="1" data-animation-delay="0.4">
                                    <a href="/html/#" title=""><img src="/html/http://placehold.it/270x192" alt="" width="270" height="192" /></a>
                                </figure>
                                <div class="details">
                                    <h4>Fly in Comfort</h4>
                                    <p>Nunc cursus libero purus ac congue ar lorem cursus ut sed vitae pulvinar massa idend porta nequetiam elerisque mi id.</p>
                                    <a href="/html/#" title="" class="button">SEE ALL</a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Features section -->
            <div class="features section global-map-area parallax" data-stellar-background-ratio="0.5">
                <div class="container">
                    <div class="row image-box style7">
                        @foreach($companies as $company)
                        <div class="col-sms-6 col-sm-6 col-md-3">
                            <article class="box">
                                <figure class="middle-block">
                                    <img src="images/{{$company->photos->first()->name}}" alt="" class="middle-item" />
                                    <span class="opacity-wrapper"></span>
                                </figure>
                                <div class="details">
                                    <h4><a href="/html/#">{{$company->name}}</a></h4>
                                    <p>
                                      {{$company->description}}</p>
                                </div>
                            </article>
                        </div>
                        @endforeach
                    
                        <div class="col-sms-6 col-sm-6 col-md-3">
                             <article class="box">
                                <figure class="middle-block">
                                    <img src="/html/http://placehold.it/300x120" alt="" class="middle-item" />
                                    <span class="opacity-wrapper"></span>
                                </figure>
                                <div class="details">
                                    <h4><a href="/html/#">Why Chose Us</a></h4>
                                    <p>
                                        Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div class="col-sms-6 col-sm-6 col-md-3">
                             <article class="box">
                                <figure class="middle-block">
                                    <img src="/html/http://placehold.it/300x120" alt="" class="middle-item" />
                                    <span class="opacity-wrapper"></span>
                                </figure>
                                <div class="details">
                                    <h4><a href="/html/#">Need Help?</a></h4>
                                    <p>
                                        Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
                                    </p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>Discover</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="/html/#">Safety</a></li>
                                <li class="col-xs-6"><a href="/html/#">About</a></li>
                                <li class="col-xs-6"><a href="/html/#">Travelo Picks</a></li>
                                <li class="col-xs-6"><a href="/html/#">Latest Jobs</a></li>
                                <li class="active col-xs-6"><a href="/html/#">Mobile</a></li>
                                <li class="col-xs-6"><a href="/html/#">Press Releases</a></li>
                                <li class="col-xs-6"><a href="/html/#">Why Host</a></li>
                                <li class="col-xs-6"><a href="/html/#">Blog Posts</a></li>
                                <li class="col-xs-6"><a href="/html/#">Social Connect</a></li>
                                <li class="col-xs-6"><a href="/html/#">Help Topics</a></li>
                                <li class="col-xs-6"><a href="/html/#">Site Map</a></li>
                                <li class="col-xs-6"><a href="/html/#">Policies</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Travel News</h2>
                            <ul class="travel-news">
                                <li>
                                    <div class="thumb">
                                        <a href="/html/#">
                                            <img src="/html/http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="/html/#">Amazing Places</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">25 Sep, 2013</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="/html/#">
                                            <img src="/html/http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="/html/#">Travel Insurance</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">24 Sep, 2013</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>
                            <br />
                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email" />
                            </div>
                            <br />
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>About Travelo</h2>
                            <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massaidp nequetiam lore elerisque.</p>
                            <br />
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br />
                                <a href="/html/#" class="contact-email">help@travelo.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="/html/#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="/html/#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="/html/#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="/html/#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="/html/#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="/html/#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="/html/#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="/html/index.html" title="Travelo - home">
                            <img src="/html/images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="/html/#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2014 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="/html/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="/html/js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/html/js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="/html/js/jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="/html/js/bootstrap.min.js"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="/html/components/revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="/html/components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="/html/components/jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="/html/components/flexslider/jquery.flexslider.js"></script>

    <!-- parallax -->
    <script type="text/javascript" src="/html/js/jquery.stellar.min.js"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="/html/js/jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="/html/js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="/html/js/theme-scripts.js"></script>
    <script type="text/javascript" src="/html/js/scripts.js"></script>
   <script src="/html/js/algoliasearch.min.js"></script>
<script src="/html/js/autocomplete.min.js"></script>
<script>
var client = algoliasearch('DL1S32V96G', '4abd6bc9557ed076c71c7e837d64faf8');
var index = client.initIndex('companies');
//initialize autocomplete on search input (ID selector must match)
autocomplete('#aa-search-input',
{ hint: false }, {
    source: autocomplete.sources.hits(index, {hitsPerPage: 5}),
    //value to be displayed in input control after user's suggestion selection
    displayKey: 'name',
    //hash of templates used when rendering dataset
    templates: {
        //'suggestion' templating function used to render a single suggestion
        suggestion: function(suggestion) {
          return '<span>' +
            suggestion._highlightResult.name.value + '</span>';
        }
    }
});
</script> 


    
    <script type="text/javascript">
        tjq(document).ready(function() {
            tjq('.revolution-slider').revolution(
            {
                sliderType:"standard",
                sliderLayout:"auto",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    mouseScrollReverse:"default",
                    onHoverStop:"on",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style:"default",
                        enable:true,
                        hide_onmobile:false,
                        hide_onleave:false,
                        tmp:'',
                        left: {
                            h_align:"left",
                            v_align:"center",
                            h_offset:20,
                            v_offset:0
                        },
                        right: {
                            h_align:"right",
                            v_align:"center",
                            h_offset:20,
                            v_offset:0
                        }
                    }
                },
                visibilityLevels:[1240,1024,778,480],
                gridwidth:1170,
                gridheight:646,
                lazyType:"none",
                shadow:0,
                spinner:"spinner4",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        });
    </script>
</body>
</html>

