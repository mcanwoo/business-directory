@extends('layouts.app')

@section('content')

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Kayıtlı Markalar</h1>
			</div>
		</div><!--/.row-->
				<div class="panel-body">
						<ul class="todo-list">

							@foreach($brands as $brand)
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox-1" />
									<label for="checkbox-1">{{$brand->name  }}</label>
								</div>
								<div class="pull-right action-buttons"><a href="remove/{{$brand->id}}" class="trash">

									<em class="fa fa-trash"></em>
								</a>{{ csrf_field()}}
											{{ method_field('delete')}}</div>
							</li>
							@endforeach
							
						</ul>

					</div>
		<button type="submit" style="background-color: red;" class="btn btn-primary">Sil</button>
		<div class="row">
			<div class="col-lg-12">		




		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">İşyeri Seçiniz</h1>
				
			</div>
		</div><!--/.row-->

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

   
<form action="/addbrand" method="post" enctype="multipart/form-data" >
    {{ csrf_field() }}  
<select name="company_id" class="form-control">
	@foreach($companies as $company)

				
													<option value="{{$company->id}}" selected="selected">{{$company->name}}</option>
									
					

@endforeach
	</select>
    Firma Fotoğrafları (birden fazla eklenebilir):
    <br />
    <input type="file" name="logo" accept="image/*" />
    <br /><br />
      <label>Marka Adı</label>
    <input class="form-control" name="name" placeholder="">
    <label>Marka Açıklaması</label>
    <textarea class="form-control" name="description" rows="3"></textarea>
    <input type="submit" value="Yükle" style="background-color: #3b3b3b;" class="btn btn-primary" />
</form>





				
		<div class="row">
<div class="col-lg-12" style="height: 12px;"></div>
<div class="col-md-6" style="display: none;" id="panel">
				<div class="panel panel-orange">
					<div class="panel-heading dark-overlay">Orange Panel</div>
					<div class="panel-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut ante in sapien blandit luctus sed ut lacus. Phasellus urna est, faucibus nec ultrices placerat, feugiat et ligula. Donec vestibulum magna a dui pharetra molestie. Fusce et dui urna.</p>
					</div>
				</div>
			</div>

			
			<div class="col-lg-12">
				<!-- /.panel-->
	


				
				
			</div><!-- /.col-->
			<div class="col-sm-12">
				<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
			</div>
		</div><!-- /.row -->
	</div><!--/.main-->
	
<script type="text/javascript">
	
function success(argument) {
	// body...
document.getElementById('panel').style.display="block";

}


</script>

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	
</body>
</html>


@endsection('content')