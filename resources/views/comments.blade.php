@extends('layouts.app')

@section('content')

		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Yorumlar</h1>
			</div>
		</div><!--/.row-->
				<div class="panel-body">
						<ul class="todo-list">

							@foreach($comments as $comment)
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox-1" />
									<label for="checkbox-1">{{$comment->author  }} - {{$comment->rating}}</label>
									<p>{{$comment->content}}</p>
									<h6>{{$comment->author}}-{{$comment->email}}- {{ App\company::findOrFail($comment->company_id)->name}}</h6>
								<div class="pull-right action-buttons"><a href="delete/{{$comment->id}}" class="trash"></a>
									<a href="confirm/{{$comment->id}}" class="accept"> 
							@if ($comment->status=="pending")
									Yorumu Onayla
							@else
							Yorum onaylandı
							@endif
								</a>
								</div>

									<em class="fa fa-trash"></em>
								</a>{{ csrf_field()}}
											{{ method_field('delete')}}</div>
							</li>
							@endforeach
							
						</ul>

					</div>
		<button type="submit" style="background-color: red;" class="btn btn-primary">Sil</button>
		<div class="row">
			<div class="col-lg-12">
				<!-- /.panel-->
				
				
				
			<div class="col-sm-12">
				<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
			</div>
		</div><!-- /.row -->
	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<!-- 	<script type="text/javascript">

	    $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });

	</script> -->
</body>
</html>


@endsection