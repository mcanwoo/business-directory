<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lumino Çalışan Ekle</title>
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" >
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" >
<link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet" >
<link href="{{ asset('css/styles.css') }}" rel="stylesheet" >


<!-- 
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet"> -->
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Admin</a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-envelope"></em><span class="label label-danger">15</span>
					</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.php" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">3 mins ago</small>
										<a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
									<br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="dropdown-messages-box"><a href="profile.php" class="pull-left">
									<img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
									</a>
									<div class="message-body"><small class="pull-right">1 hour ago</small>
										<a href="#">New message from <strong>Jane Doe</strong>.</a>
									<br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-bell"></em><span class="label label-info">5</span>
					</a>
						<ul class="dropdown-menu dropdown-alerts">
							<li><a href="#">
								<div><em class="fa fa-envelope"></em> 1 New Message
									<span class="pull-right text-muted small">3 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-heart"></em> 12 New Likes
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
							<li class="divider"></li>
							<li><a href="#">
								<div><em class="fa fa-user"></em> 5 New Followers
									<span class="pull-right text-muted small">4 mins ago</span></div>
							</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">
@if (Auth::user())
			{{Auth::user()->name}}		
@else
Guest
	@endif</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="/home"><em class="fa fa-home">&nbsp;</em> Anasayfa</a></li>
			
			<li><a href="/add"><em class="fa fa-plus">&nbsp;</em> İşyeri Ekle</a></li>
			<li  ><a href="/addbrand"><em class="fa fa-plus">&nbsp;</em>Marka Ekle</a></li>
			<li  ><a href="/addproduct"><em class="fa fa-plus">&nbsp;</em>Ürün Ekle</a></li>
			<li  ><a href="/addservice"><em class="fa fa-plus">&nbsp;</em>Hizmet Ekle</a></li>
			<li  ><a href="/ddpromotion"><em class="fa fa-plus">&nbsp;</em>Kampanya Ekle</a></li>
			<li><a href="/addphotos"><em class="fa fa-photo">&nbsp;</em>Fotoğrafları  Düzenle</a></li>
			<li><a href="/comment"><em class="fa fa-comment">&nbsp;</em> Yorumları Yönet</a></li>
			
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-navicon">&nbsp;</em> Multilevel <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 1
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 2
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 3
					</a></li>
				</ul>
			</li>
			<li><a href="/logout"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Forms</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">İşyerini Düzenle</h1>
			</div>
		</div><!--/.row-->

		
		<div class="row">
			<div class="col-lg-12">
				<!-- /.panel-->
				
				
				<div class="panel panel-default">
					<div class="panel-heading">İşyeri Bilgileri</div>
					<div class="panel-body">
						<div class="col-md-6">
							<form action="{{$company->id}}" method="post" role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
								<div class="form-group">
									<label>İşyeri Adı</label>
									<input class="form-control"   value="{{$company->name}}"   name="name"  placeholder="">
								</div>
								
								
										<div class="form-group">
									<label>Telefon no </label>
									<input type="tel"   value="{{$company->phone1}}"   class="form-control" name="phone1" placeholder="05..">
								</div>
								<div class="form-group">
									<label>Telefon no 2</label>
									<input type="tel"    value="{{$company->phone2}}"  class="form-control" name="phone2" placeholder="0258..">
								</div>
								<div class="form-group">
									<label>FAX</label>
									<input type="tel"   value="{{$company->fax}}"   class="form-control" name="fax" placeholder="+91..">
								</div>
								
							<div class="form-group">
									<label>İnternet Sitesi</label>
									<input type="text"   value="{{$company->website}}"   class="form-control" name="website" placeholder="">
								</div>
								<div class="form-group">
									<label>Facebook Adresi</label>
									<input type="text"   value="{{$company->facebook}}"   class="form-control" name="facebook" placeholder="">
								</div>
								<div class="form-group">
									<label>Twitter Adresi</label>
									<input type="text"   value="{{$company->twitter}}"   class="form-control" name="twitter" placeholder="">
								</div>
								<div class="form-group">
									<label>İnstagram Adresi</label>
									<input type="text"    value="{{$company->instagram}}"  class="form-control" name="instagram" placeholder="">
								</div>
								<div class="form-group">
									<label>Youtube Tanıtım videosu linki</label>
									<input type="text"   value="{{$company->promo}}"   class="form-control" name="promo" placeholder="">
								</div>
								
								<div class="form-group">
									<label>Çalışma saatleri</label>
<input type="time" id="appt" name="workstart_hour"
       min="0:00"   value="{{$company->workstart_hour}}"   max="24:00" value="09:00" >

								</div>

								<div class="form-group">
									
<input type="time"   value="{{$company->workend_hour}}"   id="appt" name="workend_hour"
       min="0:00" max="24:00"  value="18:00">

								</div>
								<div class="form-group">
									<label>Firma Açıklaması</label>
									<textarea class="form-control"     name="description" rows="3">{{$company->description}}</textarea>
								</div>
								<div class="form-group">
									<label>Firma Vizyonu</label>
									<textarea class="form-control"     name="vizyon" rows="3">{{$company->vizyon}}</textarea>
								</div>
								<div class="form-group">
									<label>Firma Misyonu</label>
									<textarea class="form-control"     name="misyon" rows="3">{{$company->misyon}}</textarea>
								</div>
								<div class="form-group">
									<label>Adresi</label>
									<textarea class="form-control"     name="adress" rows="3">{{$company->adress}}</textarea>
								</div>
								<div class="form-group">
									<label>adres tarifi</label>
									<textarea class="form-control"     name="directions" rows="3">{{$company->directions}}</textarea>
								</div>
								<div class="form-group">
									<label>Mail adresi</label>
									<input type="email" class="form-control"    value="{{$company->email}}"  name="email" placeholder="">
								</div>
								<div class="form-group">
									<label>Etiketler</label>
									<input type="text" class="form-control"   value="{{$company->tags}}"   name="tags" placeholder="">
								</div>

								<div class="form-group">
									<label>Çalışma alanları</label>
									<input type="text" class="form-control"    value="{{$company->fields}}"  name="fields" placeholder="">
								</div>

								
			
								
								
								
								
							
								
									
									
									<input type="submit" value="Kaydet" class="btn btn-primary"></button>
									<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
								
							</form>
						</div>
					</div>
				</div><!-- /.panel-->
			</div><!-- /.col-->
			<div class="col-sm-12">
				<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
			</div>
		</div><!-- /.row -->
	</div><!--/.main-->
	

	<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
		<script src="{{ asset('js/chart.min.js') }}"></script>
			<script src="{{ asset('js/chart-data.js') }}"></script>
				<script src="{{ asset('js/easypiechart.js') }}"></script>
					<script src="{{ asset('js/easypiechart-data.js') }}"></script>
						<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
						

	<!-- 	<script type="text/javascript">

	    $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });

	</script> -->
</body>
</html>
