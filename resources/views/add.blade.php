@extends('layouts.app')

@section('content')

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Kayıtlı İşyerleri</h1>
			</div>
		</div><!--/.row-->
				<div class="panel-body">
						<ul class="todo-list">

							@foreach($companies as $company)
							<li class="todo-list-item">
								<div class="checkbox">
									<input type="checkbox" id="checkbox-1" />
									<label for="checkbox-1">{{$company->name  }}</label>
								</div>

								

								<div class="pull-right action-buttons"><a href="destroy/{{$company->id}}" class="trash">
									<em class="fa fa-trash"></em>
								</a>{{ csrf_field()}}
											{{ method_field('delete')}}</div>

<div style="margin-right: 2%;" class="pull-right action-buttons"><a href="edit/{{$company->id}}" class="trash">
									<em style="color:black;" class="fa fa-edit"></em>
								</a>{{ csrf_field()}}
											{{ method_field('delete')}}</div>

							</li>
							@endforeach
							
						</ul>

					</div>
		<button type="submit" style="background-color: red;" class="btn btn-primary">Sil</button>
		<div class="row">
			<div class="col-lg-12">
				<!-- /.panel-->
				
				
				<div class="panel panel-default">
					<div class="panel-heading">İşyeri Ekle</div>
					<div class="panel-body">
						<div class="col-md-6">
							<form action="add" method="post" role="form" enctype="multipart/form-data">
							{{ csrf_field() }}
								<div class="form-group">
									<label>İşyeri Adı</label>
									<input class="form-control" name="name" placeholder="">
								</div>
								
								
										<div class="form-group">
									<label>Telefon no </label>
									<input type="tel" class="form-control" name="phone1" placeholder="05..">
								</div>
								<div class="form-group">
									<label>Telefon no 2</label>
									<input type="tel" class="form-control" name="phone2" placeholder="0258..">
								</div>
								<div class="form-group">
									<label>FAX</label>
									<input type="tel" class="form-control" name="fax" placeholder="+91..">
								</div>
								
							<div class="form-group">
									<label>İnternet Sitesi</label>
									<input type="text" class="form-control" name="website" placeholder="">
								</div>
								<div class="form-group">
									<label>Facebook Adresi</label>
									<input type="text" class="form-control" name="facebook" placeholder="">
								</div>
								<div class="form-group">
									<label>Twitter Adresi</label>
									<input type="text" class="form-control" name="twitter" placeholder="">
								</div>
								<div class="form-group">
									<label>İnstagram Adresi</label>
									<input type="text" class="form-control" name="instagram" placeholder="">
								</div>
								<div class="form-group">
									<label>Youtube Tanıtım videosu linki</label>
									<input type="text" class="form-control" name="promo" placeholder="">
								</div>
								
								<div class="form-group">
									<label>Çalışma saatleri</label>
<input type="time" id="appt" name="workstart_hour"
       min="0:00" max="24:00" value="09:00" >

								</div>

								<div class="form-group">
									
<input type="time" id="appt" name="workend_hour"
       min="0:00" max="24:00"  value="18:00">

								</div>
								<div class="form-group">
									<label>Firma Açıklaması</label>
									<textarea class="form-control" name="description" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>Firma Vizyonu</label>
									<textarea class="form-control" name="vizyon" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>Firma Misyonu</label>
									<textarea class="form-control" name="misyon" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>Adresi</label>
									<textarea class="form-control" name="adress" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>adres tarifi</label>
									<textarea class="form-control" name="directions" rows="3"></textarea>
								</div>
								<div class="form-group">
									<label>Mail adresi</label>
									<input type="email" class="form-control" name="email" placeholder="">
								</div>
								<div class="form-group">
									<label>Etiketler</label>
									<input type="text" class="form-control" name="tags" placeholder="">
								</div>

								<div class="form-group">
									<label>Çalışma alanları</label>
									<input type="text" class="form-control" name="fields" placeholder="">
								</div>

								
			
								
								
								
								
							
								
									
									
									<input type="submit" value="Kaydet" class="btn btn-primary"></button>
									<!-- <button type="reset" class="btn btn-default">Reset Button</button> -->
								
							</form>
						</div>
					</div>
				</div><!-- /.panel-->
			</div><!-- /.col-->
			<div class="col-sm-12">
				<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
			</div>
		</div><!-- /.row -->
	</div><!--/.main-->
	
<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<!-- 	<script type="text/javascript">

	    $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });

	</script> -->
</body>
</html>


@endsection