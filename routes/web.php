<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'publicController@welcome');

Route::get('/logout', function () {
    
Auth::logout();
    return view('welcome');
});


// Route::get('add', function () {
//     // Only authenticated users
//     // 7may enter...
//      return view('add');
// })->middleware('auth');

Route::post('search','publicController@search');

Route::get('add','companyController@show')->middleware('auth');
Route::get('comment','commentController@show')->middleware('auth');


Route::post('add','companyController@store')->middleware('auth');

Route::post('upload','photoController@store')->middleware('auth');
Route::post('addbrand','brandController@store')->middleware('auth');
Route::post('addproduct','productController@store')->middleware('auth');
Route::post('addservice','serviceController@store')->middleware('auth');
Route::post('addpromotion','promotionController@store')->middleware('auth');
Route::post('/addcomment', 'commentController@store');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/addphotos', 'HomeController@photos');

Route::get('/addbrand', 'HomeController@brands');
Route::get('/addproduct', 'HomeController@product');
Route::get('/addservice', 'HomeController@service');
Route::get('/addpromotion', 'HomeController@promotion');

Route::post('edit/{id}', 'HomeController@update')->middleware('auth');

Route::get('edit/{id}', 'HomeController@edit')->middleware('auth');
Route::get('destroy/{id}', 'companyController@destroy')->middleware('auth');
Route::get('confirm/{id}', 'commentController@confirm')->middleware('auth');
Route::get('delete/{id}', 'commentController@destroy')->middleware('auth');
Route::get('remove/{id}', 'brandController@destroy')->middleware('auth');
Route::get('erase/{id}', 'serviceController@destroy')->middleware('auth');
Route::get('kill/{id}', 'productController@destroy')->middleware('auth');
Route::get('dump/{id}', 'promotionController@destroy')->middleware('auth');
Route::get('firma/{id}', 'publicController@create');